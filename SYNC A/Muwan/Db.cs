﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using NLog;

namespace Muwan
{
    class Db
    {
        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        MySqlConnection mysqlConnection;

        /// <summary>
        ///     Init
        ///     Bootstraps the necessary tables and triggers
        ///     It makes sure that these are available on the client's local computer
        /// </summary>
        public static void Init()
        {
            logger.Info("==>Db Init method , bootstraping the db \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    //create tally table
                    Setting.GetInstance().CreateTallyTableQuery;

                    //create adapter table if id type is auto
                    if (Setting.GetInstance().IdType == Setting.IdTypes.auto)
                    {
                        Setting.GetInstance().CreateAdapterTableQuery;
                    }

                    //create triggers on target tables to be synchronised
                    string triggerQuery = Setting.GetInstance().TrigerQuery;
                    foreach (Target target in Setting.GetInstance().TargetTables)
                    {
                        string sql = triggerQuery.Replace("tableName",target.TableName);
                        sql;
                    }
                    
                }
                catch (Exception ex)
                {
                    string info = "Exception while bootstraping db :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                }finally{
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }
        }

        public void Connect()
        {
            logger.Info("==>Db connection method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                try
                {
                    mysqlConnection = new MySqlConnection();
                    mysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    mysqlConnection.Open();
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    string info = "Connecting to the database failed:" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                }
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli))){           
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }
        }

        public void Disconnect()
        {
            logger.Info("==>Db disconnection method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }
        }
    }
}
