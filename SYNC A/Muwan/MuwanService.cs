﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Muwan
{
    public partial class MuwanService : ServiceBase
    {
        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public MuwanService()
        {
            InitializeComponent();
            logger.Info("==>Created the service object");
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("==>Service started");
            SetUp();
        }

        protected override void OnStop()
        {
            logger.Info("==>Service stopped");
            Finish();
        }

        protected override void OnPause()
        {
            logger.Info("==>Service paused");
            base.OnPause();
            Finish();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            logger.Info("==>Service continued");
            SetUp();
        }

        private void SetUp()
        {
            //first load in the settings
            Setting.GetInstance().LoadSettings();           
            //bootstrap the db
            Db.Init();
            //set timer intervals
            TallyWatcher.Interval = Setting.GetInstance().TallyWatcherInterval;
            DraftWatcher.Interval = Setting.GetInstance().DraftWatcherInterval;
            //enable the timers
            TallyWatcher.Enabled = true;
            DraftWatcher.Enabled = true;
            //start the timers
            TallyWatcher.Start();
            DraftWatcher.Start();

        }

        private void Finish()
        {
            //stop the timmers
            TallyWatcher.Stop();
            DraftWatcher.Stop();
            //disable the timers
            TallyWatcher.Enabled = false;
            DraftWatcher.Enabled = false;
            //dispose the settings object
            Setting.DisposeInstance();
        }

        private void TallyWatcher_Tick(object sender, EventArgs e)
        {
            String logStr = "==>Tally watcher action \n";
            logger.Info(logStr);
            //connect to the data base
            Db db = new Db();
            db.Connect();
            //use the database

            db.Disconnect();
            logger.Info(logStr);

        }


    }
}
