﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muwan
{
    class SettingsModel
    {
        public string DatabaseUser { get; set; }
        public string IdType { get; set; }

        public int TallyWatcherInterval { get; set; }
        public int DraftWatcherInterval { get; set; }

        public string BaseDomain { get; set; }
        public string PersisterURL { get; set; }
        public string FetcherURL { get; set; }

        public string DataBaseType { get; set; }
        public string ConnectionString { get; set; }
        public string TallyTableName { get; set; }
        public string AdapterTableName { get; set; }

        public string CreateTallyTableQuery { get; set; }
        public string CreateAdapterTableQuery { get; set; }

        public List<Target> TargetTables { get; set; }
        public string TrigerQuery { get; set; }
    }
}
