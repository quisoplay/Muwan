﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muwan
{
    class Setting 
    {

        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string DatabaseUser { get { return this.SettingsModel.DatabaseUser; } }

        public int TallyWatcherInterval { get { return this.SettingsModel.TallyWatcherInterval; } }
        public int DraftWatcherInterval { get { return this.SettingsModel.DraftWatcherInterval; } }

        public string BaseDomain { get { return this.SettingsModel.BaseDomain; } }
        public string PersisterURL { get { return this.SettingsModel.PersisterURL; } }
        public string FetcherURL { get { return this.SettingsModel.FetcherURL; } }

        public string DataBaseType { get { return this.SettingsModel.DataBaseType; } }
        public string ConnectionString { get { return this.SettingsModel.ConnectionString; } }
        public string TallyTableName { get { return this.SettingsModel.TallyTableName; } }
        public string AdapterTableName { get { return this.SettingsModel.AdapterTableName; } }

        public string CreateTallyTableQuery { 
            get {
                string tableName = this.SettingsModel.TallyTableName;
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CreateTallyTableQuery))
                {
                    query = this.SettingsModel.CreateTallyTableQuery.Replace("tallyTableName", TallyTableName);
                }
                return query;
            } 
        }

        public string CreateAdapterTableQuery
        {
            get
            {
                string tableName = this.SettingsModel.AdapterTableName;
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CreateAdapterTableQuery))
                {
                    query = this.SettingsModel.CreateAdapterTableQuery.Replace("adapterTableName", AdapterTableName);
                }
                return query;
            }
        }

        public List<Target> TargetTables { get { return this.SettingsModel.TargetTables; } }
        public string TrigerQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.TrigerQuery))
                {
                    query = this.SettingsModel.TrigerQuery.Replace("dataBaseUser", DatabaseUser);
                }
                return query;
            }
        }

        /// <summary>
        /// The id type is important because the if it is guid
        /// then there is no need for the adapter table and there for
        /// no need for the muwan translation step
        /// </summary>
        public IdTypes IdType { 
            get { 
                if(string.IsNullOrEmpty(this.SettingsModel.IdType)){
                    return IdTypes.auto;
                }

                if (this.SettingsModel.IdType.Equals(Convert.ToString(Setting.IdTypes.auto)))
                {
                    return IdTypes.auto;
                }

                if(this.SettingsModel.IdType.Equals( Convert.ToString(Setting.IdTypes.guid) )){
                    return IdTypes.guid;
                }

                return IdTypes.auto;
            } 
        }

        public enum DataBaseTypes
        {
            mssql = 1,
            mysql = 2,
            mysqli = 3,
            psql = 4,
            oracle = 5
        };

        public enum IdTypes
        {
            auto = 1,
            guid = 2
        };

        //atomic parttern
        private static Setting Instance;

        public static Setting GetInstance (){            
            if (Setting.Instance == null)
            {
                Setting.Instance = new Setting();
            }
            return Setting.Instance;           
        }

        public static void DisposeInstance()
        {
            Setting.Instance = null;          
        }

        private SettingsModel SettingsModel;

        private Setting()
        {
            this.SettingsModel = new SettingsModel();
            logger.Info("==>Created the settings object");
        }

        public void LoadSettings()
        {
            logger.Info("==>Loading settings");

            string fileName = "setting.json";
            string currentDirectory = System.Environment.CurrentDirectory;
            string filePath = Path.Combine(currentDirectory, fileName);
            string i = "Path for settings is : " + filePath;
            logger.Info(i);
            //read in the json file
            if(File.Exists(filePath)){
                FileStream file = File.Open(filePath, FileMode.Open);
                StreamReader sr = new StreamReader(file);
                string settingString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                i = "The follwing settings were found : " + settingString;
                logger.Info(i);
                SettingsModel model =  JsonConvert.DeserializeObject<SettingsModel>(settingString);
                this.SettingsModel = model;
                logger.Info("Settings loaded successfully");
                logger.Info("Validating settings");
                this.validate();
            }else{
                logger.Info("Settings file was not found");
            }
        }

        public void validate()
        {
            //check if there are any target tables 
            if (this.SettingsModel.TargetTables == null || this.SettingsModel.TargetTables.Count() == 0)
            {
                logger.Info("Exception in settings, There are not tables to synchronise");
                throw new Exception("Exception in setup, There are not tables to synchronise");
            }

            List<string> targetNames = new List<string>();
            foreach (Target target in this.SettingsModel.TargetTables)
            {
                targetNames.Add(target.TableName);
            }

            //check that these targets are fine
            foreach (Target target in this.SettingsModel.TargetTables)
            {
                //a target must have a name and a unique colum name
                if (string.IsNullOrEmpty(target.TableName))
                {
                    string info = "Exception in settings, One of the Target table names is not specified";
                    logger.Info(info);
                    throw new Exception("Exception in setup, One of the Target table names is not specified");
                }

                if (string.IsNullOrEmpty(target.TableName))
                {
                    string info = "Exception in settings, no unique column name has been supplied for target " + target.TableName;
                    logger.Info(info);
                    throw new Exception(info);
                }

                //if there are any foreign keys, then they must be tables that are also going to be synchronised
                //this is important for the adapter and the translation step of the muwan service
                if (target.ForeignKeys != null && target.ForeignKeys.Count() > 0)
                {
                    foreach (Parent parent in target.ForeignKeys)
                    {
                        //this parent key must have a column name
                        if (string.IsNullOrEmpty(parent.ColumnName))
                        {
                            string info = "Exception in settings, no column name has been supplied for parent of  " + target.TableName;
                            logger.Info(info);
                            throw new Exception(info);
                        }
                        //the table name of the parent must also be listed for synchronisation
                        if (!targetNames.Contains(parent.TableName))
                        {
                            string info = "Exception in settings, the parent table specified for this foreign key " +
                                           parent.ColumnName + " of target " + target.TableName + " is not listed for synchronisation" ;
                            logger.Info(info);
                            throw new Exception(info);
                        }
                    }
                }
            }

            //the database user must be specified
            if (string.IsNullOrEmpty(this.SettingsModel.DatabaseUser))
            {
                string info = "Exception in settings, the muwan database user has not been specified  ";
                logger.Info(info);
                throw new Exception(info);
            }

            //validate the id type has been set
            if (string.IsNullOrEmpty(this.SettingsModel.IdType))
            {
                string info = "Exception in settings, the id type is not specified ";
                logger.Info(info);
                throw new Exception(info);
            }
        }


    }
}
