-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 06:09 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_book_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `dde`
--

DROP TABLE IF EXISTS `dde`;
CREATE TABLE IF NOT EXISTS `dde` (
  `de` int(30) NOT NULL AUTO_INCREMENT,
  `dfdf` varchar(60) NOT NULL DEFAULT 'flood',
  `tried` timestamp NOT NULL,
  PRIMARY KEY (`de`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dde`
--

INSERT INTO `dde` (`de`, `dfdf`, `tried`) VALUES
(1, 'flood', '2016-03-29 05:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class`
--

DROP TABLE IF EXISTS `muwan_academic_class`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_room_id` int(30) NOT NULL,
  `academic_year_id` int(30) NOT NULL,
  `name` varchar(69) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_class`
--

INSERT INTO `muwan_academic_class` (`id`, `class_room_id`, `academic_year_id`, `name`, `other`) VALUES
(1, 1, 1, 'class of 2015', '1'),
(2, 2, 1, 'class of 2015', '1');

--
-- Triggers `muwan_academic_class`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_class` AFTER DELETE ON `muwan_academic_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_class` AFTER INSERT ON `muwan_academic_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_class` AFTER UPDATE ON `muwan_academic_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class_student`
--

DROP TABLE IF EXISTS `muwan_academic_class_student`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `academic_class_id` int(30) NOT NULL,
  `student_id` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_class_student`
--

INSERT INTO `muwan_academic_class_student` (`id`, `academic_class_id`, `student_id`, `other`) VALUES
(1, 1, 1, '1'),
(2, 1, 2, '2');

--
-- Triggers `muwan_academic_class_student`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_class_student` AFTER DELETE ON `muwan_academic_class_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class_student', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_class_student` AFTER INSERT ON `muwan_academic_class_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class_student', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_class_student` AFTER UPDATE ON `muwan_academic_class_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_class_student', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_year`
--

DROP TABLE IF EXISTS `muwan_academic_year`;
CREATE TABLE IF NOT EXISTS `muwan_academic_year` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `year` int(30) NOT NULL,
  `start_date` int(50) NOT NULL,
  `end_date` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_year`
--

INSERT INTO `muwan_academic_year` (`id`, `year`, `start_date`, `end_date`) VALUES
(2, 2016, 1451595600, 1482613200),
(3, 2017, 1483218000, 1514581200);

--
-- Triggers `muwan_academic_year`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_year` AFTER DELETE ON `muwan_academic_year` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_year', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_year` AFTER INSERT ON `muwan_academic_year` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_year', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_year` AFTER UPDATE ON `muwan_academic_year` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_academic_year', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_claim`
--

DROP TABLE IF EXISTS `muwan_claim`;
CREATE TABLE IF NOT EXISTS `muwan_claim` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `end_point` varchar(100) NOT NULL,
  `affected_table` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `unique_value` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_claim`
--

INSERT INTO `muwan_claim` (`id`, `end_point`, `affected_table`, `activity`, `unique_value`, `date_created`) VALUES
(1, '1234', 'muwan_school', 'modify', '6', '2016-03-29 17:09:58'),
(2, '1234', 'muwan_school', 'input', '7', '2016-03-29 17:12:11'),
(3, '1234', 'muwan_school', 'remove', '8', '2016-03-29 17:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class`
--

DROP TABLE IF EXISTS `muwan_class`;
CREATE TABLE IF NOT EXISTS `muwan_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `rank` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_class`
--

INSERT INTO `muwan_class` (`id`, `school_id`, `name`, `rank`, `other`) VALUES
(1, 1, 'senior one', 1, '1'),
(2, 2, 'senior one', 1, '1'),
(3, 3, 'senior one', 1, '2'),
(5, 2, 'senior two', 2, '1'),
(6, 3, 'senior two', 2, '2');

--
-- Triggers `muwan_class`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_class` AFTER DELETE ON `muwan_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_class` AFTER INSERT ON `muwan_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_class` AFTER UPDATE ON `muwan_class` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class_room`
--

DROP TABLE IF EXISTS `muwan_class_room`;
CREATE TABLE IF NOT EXISTS `muwan_class_room` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_id` int(30) NOT NULL,
  `stream_id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_class_room`
--

INSERT INTO `muwan_class_room` (`id`, `class_id`, `stream_id`, `name`, `other`) VALUES
(1, 1, 1, 'senior one yellow', '1'),
(2, 1, 2, 'senior one blue', '1'),
(3, 2, 3, 'senior one apples', '1'),
(4, 2, 4, 'senior one oranges', '1'),
(5, 3, 5, 'senior one planes', '2'),
(6, 3, 6, 'senior one jets', '2');

--
-- Triggers `muwan_class_room`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_class_room` AFTER DELETE ON `muwan_class_room` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class_room', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_class_room` AFTER INSERT ON `muwan_class_room` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class_room', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_class_room` AFTER UPDATE ON `muwan_class_room` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_class_room', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_draft`
--

DROP TABLE IF EXISTS `muwan_draft`;
CREATE TABLE IF NOT EXISTS `muwan_draft` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `affected_table` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `unique_value` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_draft`
--

INSERT INTO `muwan_draft` (`id`, `affected_table`, `activity`, `unique_value`, `date_created`) VALUES
(1, 'muwan_school', 'modify', '6', '2016-03-29 17:09:58'),
(2, 'muwan_school', 'input', '7', '2016-03-29 17:12:11'),
(3, 'muwan_school', 'modify', '7', '2016-03-29 17:12:27'),
(4, 'muwan_school', 'input', '8', '2016-03-29 17:12:53'),
(5, 'muwan_school', 'remove', '8', '2016-03-29 17:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_progress`
--

DROP TABLE IF EXISTS `muwan_progress`;
CREATE TABLE IF NOT EXISTS `muwan_progress` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `end_point` varchar(100) NOT NULL,
  `draft_id` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_progress`
--

INSERT INTO `muwan_progress` (`id`, `end_point`, `draft_id`, `date_created`) VALUES
(1, '1234', '4', '2016-03-29 17:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_school`
--

DROP TABLE IF EXISTS `muwan_school`;
CREATE TABLE IF NOT EXISTS `muwan_school` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `branch_id` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_school`
--

INSERT INTO `muwan_school` (`id`, `name`, `description`, `branch_id`) VALUES
(1, 'The Makere College School', 'A secondary school', 1),
(2, 'Mengo Senior Secondary School', 'Another secondary school', 1),
(3, 'Old Kampala Senior Secondary School', 'Yet another secondary school', 2),
(4, 'Lubiri Secondary school', 'another senior secondary school', 1),
(6, 'quisogoo', 'wwwwwwwwwwww', 1),
(7, 'Bweyogere primary school', 'the best school', 1);

--
-- Triggers `muwan_school`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_school` AFTER DELETE ON `muwan_school` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_school', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_school` AFTER INSERT ON `muwan_school` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_school', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_school` AFTER UPDATE ON `muwan_school` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_school', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_stream`
--

DROP TABLE IF EXISTS `muwan_stream`;
CREATE TABLE IF NOT EXISTS `muwan_stream` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_stream`
--

INSERT INTO `muwan_stream` (`id`, `school_id`, `name`, `description`) VALUES
(1, 1, 'yellow', 'the yellow stream'),
(2, 1, 'blue', 'the blue stream'),
(3, 2, 'apples', 'the apple stream'),
(4, 2, 'oranges', 'the oranges stream'),
(5, 3, 'planes', 'the planes stream'),
(6, 3, 'jets', 'the jets stream'),
(7, 1, 'pink', 'the pink stream');

--
-- Triggers `muwan_stream`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_stream` AFTER DELETE ON `muwan_stream` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_stream', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_stream` AFTER INSERT ON `muwan_stream` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_stream', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_stream` AFTER UPDATE ON `muwan_stream` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_stream', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_student`
--

DROP TABLE IF EXISTS `muwan_student`;
CREATE TABLE IF NOT EXISTS `muwan_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `date_of_birth` int(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_student`
--

INSERT INTO `muwan_student` (`id`, `first_name`, `last_name`, `date_of_birth`, `gender`, `other`) VALUES
(1, 'nyola', 'mike', 629326800, 'male', '1'),
(2, 'Albert', 'Eistein', 629672400, 'male', '2');

--
-- Triggers `muwan_student`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_student` AFTER DELETE ON `muwan_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_student', 'remove', OLD.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_student` AFTER INSERT ON `muwan_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_student', 'input', NEW.id);
	 			
	 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_student` AFTER UPDATE ON `muwan_student` FOR EACH ROW BEGIN
	 			
	 				INSERT INTO muwan_draft
	 				( affected_table, activity, unique_value ) VALUES 
	 				( 'muwan_student', 'modify', NEW.id);
	 			
	 		END
$$
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
