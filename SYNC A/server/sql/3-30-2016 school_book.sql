-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 06:08 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `dde`
--

DROP TABLE IF EXISTS `dde`;
CREATE TABLE IF NOT EXISTS `dde` (
  `de` int(30) NOT NULL AUTO_INCREMENT,
  `dfdf` varchar(60) NOT NULL DEFAULT 'flood',
  `tried` timestamp NOT NULL,
  PRIMARY KEY (`de`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class`
--

DROP TABLE IF EXISTS `muwan_academic_class`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_room_id` int(30) NOT NULL,
  `academic_year_id` int(30) NOT NULL,
  `name` varchar(69) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_class`
--

INSERT INTO `muwan_academic_class` (`id`, `class_room_id`, `academic_year_id`, `name`, `other`) VALUES
(1, 1, 1, 'class of 2015', '1'),
(2, 2, 1, 'class of 2015', '1');

--
-- Triggers `muwan_academic_class`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_class` AFTER DELETE ON `muwan_academic_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_class` AFTER INSERT ON `muwan_academic_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_class`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_class` AFTER UPDATE ON `muwan_academic_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class_student`
--

DROP TABLE IF EXISTS `muwan_academic_class_student`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `academic_class_id` int(30) NOT NULL,
  `student_id` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_class_student`
--

INSERT INTO `muwan_academic_class_student` (`id`, `academic_class_id`, `student_id`, `other`) VALUES
(1, 1, 1, '1');

--
-- Triggers `muwan_academic_class_student`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_class_student` AFTER DELETE ON `muwan_academic_class_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class_student', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_class_student` AFTER INSERT ON `muwan_academic_class_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class_student', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_class_student`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_class_student` AFTER UPDATE ON `muwan_academic_class_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_class_student', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_year`
--

DROP TABLE IF EXISTS `muwan_academic_year`;
CREATE TABLE IF NOT EXISTS `muwan_academic_year` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `year` int(30) NOT NULL,
  `start_date` int(50) NOT NULL,
  `end_date` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_academic_year`
--

INSERT INTO `muwan_academic_year` (`id`, `year`, `start_date`, `end_date`) VALUES
(1, 2016, 1451595600, 1482613200),
(2, 2017, 1483218000, 1514581200);

--
-- Triggers `muwan_academic_year`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_academic_year` AFTER DELETE ON `muwan_academic_year` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_year', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_academic_year` AFTER INSERT ON `muwan_academic_year` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_year', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_academic_year`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_academic_year` AFTER UPDATE ON `muwan_academic_year` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_academic_year', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_adapter`
--

DROP TABLE IF EXISTS `muwan_adapter`;
CREATE TABLE IF NOT EXISTS `muwan_adapter` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `local_table_unique_value` varchar(100) DEFAULT NULL,
  `foreign_table_unique_value` varchar(100) DEFAULT NULL,
  `local_deleted` varchar(100) DEFAULT 'no',
  `foreign_deleted` varchar(100) DEFAULT 'no',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_adapter`
--

INSERT INTO `muwan_adapter` (`id`, `table_name`, `local_table_unique_value`, `foreign_table_unique_value`, `local_deleted`, `foreign_deleted`, `date_created`) VALUES
(1, 'muwan_academic_year', '1', '2', 'no', 'no', '2016-03-29 17:05:14'),
(2, 'muwan_academic_year', '2', '3', 'no', 'no', '2016-03-29 17:05:14'),
(3, 'muwan_school', '1', '1', 'no', 'no', '2016-03-29 17:08:28'),
(4, 'muwan_school', '2', '2', 'no', 'no', '2016-03-29 17:08:29'),
(5, 'muwan_school', '3', '4', 'no', 'no', '2016-03-29 17:08:29'),
(6, 'muwan_school', '4', '6', 'no', 'no', '2016-03-29 17:08:29'),
(7, 'muwan_class', '1', '1', 'no', 'no', '2016-03-29 17:08:34'),
(8, 'muwan_class', '2', '2', 'no', 'no', '2016-03-29 17:08:34'),
(9, 'muwan_class', '3', '5', 'no', 'no', '2016-03-29 17:08:34'),
(10, 'muwan_stream', '1', '1', 'no', 'no', '2016-03-29 17:08:35'),
(11, 'muwan_stream', '2', '2', 'no', 'no', '2016-03-29 17:08:35'),
(12, 'muwan_stream', '3', '3', 'no', 'no', '2016-03-29 17:08:35'),
(13, 'muwan_stream', '4', '4', 'no', 'no', '2016-03-29 17:08:35'),
(14, 'muwan_stream', '5', '1', 'no', 'no', '2016-03-29 17:09:20'),
(15, 'muwan_stream', '6', '2', 'no', 'no', '2016-03-29 17:09:20'),
(16, 'muwan_stream', '7', '3', 'no', 'no', '2016-03-29 17:09:20'),
(17, 'muwan_stream', '8', '4', 'no', 'no', '2016-03-29 17:09:20'),
(18, 'muwan_stream', '9', '5', 'no', 'no', '2016-03-29 17:09:20'),
(19, 'muwan_stream', '10', '6', 'no', 'no', '2016-03-29 17:09:21'),
(20, 'muwan_stream', '11', '7', 'no', 'no', '2016-03-29 17:09:21'),
(21, 'muwan_class_room', '1', '1', 'no', 'no', '2016-03-29 17:09:21'),
(22, 'muwan_class_room', '2', '2', 'no', 'no', '2016-03-29 17:09:21'),
(23, 'muwan_class_room', '3', '3', 'no', 'no', '2016-03-29 17:09:21'),
(24, 'muwan_class_room', '4', '4', 'no', 'no', '2016-03-29 17:09:21'),
(25, 'muwan_academic_class', '1', '1', 'no', 'no', '2016-03-29 17:09:22'),
(26, 'muwan_academic_class', '2', '2', 'no', 'no', '2016-03-29 17:09:22'),
(27, 'muwan_student', '1', '1', 'no', 'no', '2016-03-29 17:09:22'),
(28, 'muwan_student', '2', '2', 'no', 'no', '2016-03-29 17:09:22'),
(29, 'muwan_academic_class_student', '1', '1', 'no', 'no', '2016-03-29 17:09:22'),
(30, 'muwan_school', '5', '7', 'no', 'no', '2016-03-29 17:12:11'),
(31, 'muwan_school', '6', '8', 'yes', 'yes', '2016-03-29 17:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_claim`
--

DROP TABLE IF EXISTS `muwan_claim`;
CREATE TABLE IF NOT EXISTS `muwan_claim` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `end_point` varchar(100) NOT NULL,
  `affected_table` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `unique_value` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class`
--

DROP TABLE IF EXISTS `muwan_class`;
CREATE TABLE IF NOT EXISTS `muwan_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `rank` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_class`
--

INSERT INTO `muwan_class` (`id`, `school_id`, `name`, `rank`, `other`) VALUES
(1, 1, 'senior one', 1, '1'),
(2, 2, 'senior one', 1, '1'),
(3, 2, 'senior two', 2, '1');

--
-- Triggers `muwan_class`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_class` AFTER DELETE ON `muwan_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_class` AFTER INSERT ON `muwan_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_class`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_class` AFTER UPDATE ON `muwan_class` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class_room`
--

DROP TABLE IF EXISTS `muwan_class_room`;
CREATE TABLE IF NOT EXISTS `muwan_class_room` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_id` int(30) NOT NULL,
  `stream_id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_class_room`
--

INSERT INTO `muwan_class_room` (`id`, `class_id`, `stream_id`, `name`, `other`) VALUES
(1, 1, 1, 'senior one yellow', '1'),
(2, 1, 2, 'senior one blue', '1'),
(3, 2, 3, 'senior one apples', '1'),
(4, 2, 4, 'senior one oranges', '1');

--
-- Triggers `muwan_class_room`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_class_room` AFTER DELETE ON `muwan_class_room` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class_room', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_class_room` AFTER INSERT ON `muwan_class_room` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class_room', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_class_room`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_class_room` AFTER UPDATE ON `muwan_class_room` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_class_room', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_equalise`
--

DROP TABLE IF EXISTS `muwan_equalise`;
CREATE TABLE IF NOT EXISTS `muwan_equalise` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) NOT NULL,
  `last_unique_value` varchar(100) DEFAULT NULL,
  `was_equallised` varchar(30) NOT NULL DEFAULT 'false',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_equalise`
--

INSERT INTO `muwan_equalise` (`id`, `table_name`, `last_unique_value`, `was_equallised`, `date_created`) VALUES
(1, 'muwan_academic_year', '3', 'true', '2016-03-29 17:05:14'),
(2, 'muwan_school', '6', 'true', '2016-03-29 17:08:29'),
(3, 'muwan_class', '5', 'true', '2016-03-29 17:08:34'),
(4, 'muwan_stream', '7', 'true', '2016-03-29 17:09:21'),
(5, 'muwan_class_room', '4', 'true', '2016-03-29 17:09:21'),
(6, 'muwan_academic_class', '2', 'true', '2016-03-29 17:09:22'),
(7, 'muwan_student', '2', 'true', '2016-03-29 17:09:22'),
(8, 'muwan_academic_class_student', '1', 'true', '2016-03-29 17:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `muwan_school`
--

DROP TABLE IF EXISTS `muwan_school`;
CREATE TABLE IF NOT EXISTS `muwan_school` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `branch_id` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_school`
--

INSERT INTO `muwan_school` (`id`, `name`, `description`, `branch_id`) VALUES
(1, 'The Makere College School', 'A secondary school', 1),
(2, 'Mengo Senior Secondary School', 'Another secondary school', 1),
(3, 'Lubiri Secondary school', 'another senior secondary school', 1),
(4, 'quisogoo', 'wwwwwwwwwwww', 1),
(5, 'Bweyogere primary school', 'the best school', 1);

--
-- Triggers `muwan_school`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_school` AFTER DELETE ON `muwan_school` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_school', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_school` AFTER INSERT ON `muwan_school` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_school', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_school`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_school` AFTER UPDATE ON `muwan_school` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_school', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_stream`
--

DROP TABLE IF EXISTS `muwan_stream`;
CREATE TABLE IF NOT EXISTS `muwan_stream` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_stream`
--

INSERT INTO `muwan_stream` (`id`, `school_id`, `name`, `description`) VALUES
(1, 1, 'yellow', 'the yellow stream'),
(2, 1, 'blue', 'the blue stream'),
(3, 2, 'apples', 'the apple stream'),
(4, 2, 'oranges', 'the oranges stream'),
(5, 1, 'yellow', 'the yellow stream'),
(6, 1, 'blue', 'the blue stream'),
(7, 2, 'apples', 'the apple stream'),
(8, 2, 'oranges', 'the oranges stream'),
(9, 3, 'planes', 'the planes stream'),
(10, 3, 'jets', 'the jets stream'),
(11, 1, 'pink', 'the pink stream');

--
-- Triggers `muwan_stream`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_stream` AFTER DELETE ON `muwan_stream` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_stream', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_stream` AFTER INSERT ON `muwan_stream` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_stream', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_stream`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_stream` AFTER UPDATE ON `muwan_stream` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_stream', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_student`
--

DROP TABLE IF EXISTS `muwan_student`;
CREATE TABLE IF NOT EXISTS `muwan_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `date_of_birth` int(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `muwan_student`
--

INSERT INTO `muwan_student` (`id`, `first_name`, `last_name`, `date_of_birth`, `gender`, `other`) VALUES
(1, 'nyola', 'mike', 629326800, 'male', '1'),
(2, 'Albert', 'Eistein', 629672400, 'male', '2');

--
-- Triggers `muwan_student`
--
DROP TRIGGER IF EXISTS `trigger_delete_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_delete_muwan_student` AFTER DELETE ON `muwan_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_student', 'remove', OLD.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_input_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_input_muwan_student` AFTER INSERT ON `muwan_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_student', 'input', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_update_muwan_student`;
DELIMITER $$
CREATE TRIGGER `trigger_update_muwan_student` AFTER UPDATE ON `muwan_student` FOR EACH ROW BEGIN
 			IF ( USER() != 'muwan@localhost' ) THEN 
 				INSERT INTO muwan_tally 
 				( affected_table, activity, unique_value ) VALUES 
 				( 'muwan_student', 'modify', NEW.id);
 			END IF;
 		END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_tally`
--

DROP TABLE IF EXISTS `muwan_tally`;
CREATE TABLE IF NOT EXISTS `muwan_tally` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `affected_table` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `unique_value` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
