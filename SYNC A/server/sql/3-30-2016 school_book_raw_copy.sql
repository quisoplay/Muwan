-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 06:09 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_book_raw_copy`
--

-- --------------------------------------------------------

--
-- Table structure for table `dde`
--

DROP TABLE IF EXISTS `dde`;
CREATE TABLE IF NOT EXISTS `dde` (
  `de` int(30) NOT NULL AUTO_INCREMENT,
  `dfdf` varchar(60) NOT NULL DEFAULT 'flood',
  `tried` timestamp NOT NULL,
  PRIMARY KEY (`de`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class`
--

DROP TABLE IF EXISTS `muwan_academic_class`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_room_id` int(30) NOT NULL,
  `academic_year_id` int(30) NOT NULL,
  `name` varchar(69) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_class_student`
--

DROP TABLE IF EXISTS `muwan_academic_class_student`;
CREATE TABLE IF NOT EXISTS `muwan_academic_class_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `academic_class_id` int(30) NOT NULL,
  `student_id` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_academic_year`
--

DROP TABLE IF EXISTS `muwan_academic_year`;
CREATE TABLE IF NOT EXISTS `muwan_academic_year` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `year` int(30) NOT NULL,
  `start_date` int(50) NOT NULL,
  `end_date` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_claim`
--

DROP TABLE IF EXISTS `muwan_claim`;
CREATE TABLE IF NOT EXISTS `muwan_claim` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `end_point` varchar(100) NOT NULL,
  `affected_table` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `unique_value` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class`
--

DROP TABLE IF EXISTS `muwan_class`;
CREATE TABLE IF NOT EXISTS `muwan_class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `rank` int(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_class_room`
--

DROP TABLE IF EXISTS `muwan_class_room`;
CREATE TABLE IF NOT EXISTS `muwan_class_room` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `class_id` int(30) NOT NULL,
  `stream_id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_school`
--

DROP TABLE IF EXISTS `muwan_school`;
CREATE TABLE IF NOT EXISTS `muwan_school` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `branch_id` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_stream`
--

DROP TABLE IF EXISTS `muwan_stream`;
CREATE TABLE IF NOT EXISTS `muwan_stream` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `school_id` int(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `muwan_student`
--

DROP TABLE IF EXISTS `muwan_student`;
CREATE TABLE IF NOT EXISTS `muwan_student` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `date_of_birth` int(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
