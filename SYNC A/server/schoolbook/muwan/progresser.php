<?php  
	//nyd
	//implement security access
	
	//database
	include("db_connection.php");

	//the utility 
	include("utility.php");

	//this expects endpoint, lastDraftId
	if(strcasecmp($Settings->DataBaseType, "mysql") == 0){
		//check the endpoint
		if(!isset($_POST["endpoint"])){
			$Settings->PushError("endpoint is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check the lastDraftId id is required
		if(!isset($_POST["lastDraftId"])){
			$Settings->PushError("lastDraftId is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}	

		//update the progress of this endpoint
		$sql = $Settings->GetUpdateProgressQuery($_POST["endpoint"], $_POST["lastDraftId"]);
		//respond($sql);
		try{
			
			$res = $conn->exec($sql);
			
			$records = array();

			$thisRow = array();
			$thisColumn = array(
				"Name" => "progress_report",
				"Value" => "ok"
			);
			array_push($thisRow, $thisColumn);

	    	array_push($records, $thisRow );

	    	respond($records);

    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
		
	}else{
		$Settings->PushError("Database type " . $Settings->DataBaseType . " is not yet supported");
	}

	respond(array());
?>