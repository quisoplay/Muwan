<?php

class Setup {
	public $ServerName = "localhost";
	public $DatabaseName = "school_book_main";
	public $UserName = "muwan";
	public $Password = "muwan";
	public $errors = array();
	public $DatabaseUser = "'muwan'@'%'";  
	public $CleanDatabaseUser = "muwan@localhost";    
	public $IdType = "auto";	
	public $SettingsVersion = 1.0;
	public $IsBranched = true;
	public $DataBaseType = "mysql";
	public $BranchConfig = array(
		"muwan_academic_class" => "other",
		"muwan_academic_class_student" => "other",
		"muwan_class" => "other",
		"muwan_class_room" => "other",
		"muwan_school" => "branch_id",
	);
	public $TargetTables = array(
		"muwan_academic_class" => "id",
		"muwan_academic_class_student" => "id",
		"muwan_academic_year" => "id",
		"muwan_class" => "id",
		"muwan_class_room" => "id",
		"muwan_equalise" => "id",
		"muwan_school" => "id",
		"muwan_stream" => "id",
		"muwan_student" => "id"
	);

	public $DataBaseTypes = array(
        "mssql" => 1,
        "mysql" => 2,
        "mysqli" => 3,
        "psql" => 4,
        "oracle" => 5
    );

    public $IdTypes = array(
        "auto" => 1,
        "guid" => 2
    );

    public $TallyActivities = array(
        "input" => "input",
        "modify" => "modify",
        "remove" => "remove"
    );

	public $EqualisationQuery = "SELECT * FROM tablename WHERE  tableUniqueColomnName > 'lastUniqueValue' "; 
	
	public $DraftTableName = "muwan_draft";
	public $ProgressTableName = "muwan_progress";
	public $ClaimTableName = "muwan_claim";
	
	public function GetCreateDraftTableQuery(){
		$sql = "CREATE TABLE IF NOT EXISTS `".$this->DraftTableName."` (
			  `id` int(30) NOT NULL AUTO_INCREMENT,
			  `affected_table` varchar(100) NOT NULL,
			  `activity` varchar(100) NOT NULL,
			  `unique_value` varchar(100) NOT NULL,
			  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    	";
    	return $sql;
    }

    public function GetCreateProgressTableQuery(){
		$sql = "CREATE TABLE IF NOT EXISTS `".$this->ProgressTableName."` (
			  `id` int(30) NOT NULL AUTO_INCREMENT,
			  `end_point` varchar(100) NOT NULL,
			  `draft_id` varchar(100) NOT NULL,
			  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    	";
    	return $sql;
    }

    public function GetCreateClaimTableQuery(){
		$sql = "CREATE TABLE IF NOT EXISTS `".$this->ClaimTableName."` (
			  `id` int(30) NOT NULL AUTO_INCREMENT,
			  `end_point` varchar(100) NOT NULL,
			  `affected_table` varchar(100) NOT NULL,
			  `activity` varchar(100) NOT NULL,
			  `unique_value` varchar(100) NOT NULL,
			  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    	";
    	return $sql;
    }

    public function GetClaimQuery($endpoint, $affected_table, $activity, $unique_value){
		$sql = "SELECT * FROM `".$this->ClaimTableName."` WHERE
			  end_point = '".$endpoint."' AND
			  affected_table = '".$affected_table."' AND
			  activity = '".$activity."' AND
			  unique_value = '".$unique_value."' LIMIT 1";
    	return $sql;
    }

    public function GetInsertClaimQuery($endpoint, $affected_table, $activity, $unique_value){
		$sql = "INSERT INTO `".$this->ClaimTableName."` 
			  (end_point, affected_table, activity, unique_value) VALUES (
			   '".$endpoint."', '".$affected_table."', '".$activity."', '".$unique_value."' )";
    	return $sql;
    }

    public function GetInsertTrigerQuery($tablename){
    	$uniqueColomnName = $this->TargetTables[$tablename];
		$sql = "DROP TRIGGER IF EXISTS `trigger_input_".$tablename."`; 
			CREATE TRIGGER `trigger_input_".$tablename."` AFTER INSERT ON `".$tablename."`
	 		FOR EACH ROW BEGIN
	 			
	 				INSERT INTO ".$this->DraftTableName." 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( '".$tablename."', 'input', NEW.".$uniqueColomnName.");
	 			
	 		END
	 	";
    	return $sql;
    }

 	public function GetUpdateTrigerQuery($tablename){
    	$uniqueColomnName = $this->TargetTables[$tablename];
		$sql = "DROP TRIGGER IF EXISTS `trigger_update_".$tablename."`; 
	 		CREATE TRIGGER `trigger_update_".$tablename."` AFTER UPDATE ON `".$tablename."`
	 		FOR EACH ROW BEGIN
	 			
	 				INSERT INTO ".$this->DraftTableName."
	 				( affected_table, activity, unique_value ) VALUES 
	 				( '".$tablename."', 'modify', NEW.".$uniqueColomnName.");
	 			
	 		END
	 	";
    	return $sql;
    }

    public function GetDeleteTrigerQuery($tablename){
    	$uniqueColomnName = $this->TargetTables[$tablename];
		$sql = "DROP TRIGGER IF EXISTS `trigger_delete_".$tablename."`; 
	 		CREATE TRIGGER `trigger_delete_".$tablename."` AFTER DELETE ON `".$tablename."`
	 		FOR EACH ROW BEGIN
	 			
	 				INSERT INTO ".$this->DraftTableName." 
	 				( affected_table, activity, unique_value ) VALUES 
	 				( '".$tablename."', 'remove', OLD.".$uniqueColomnName.");
	 			
	 		END	 		
	 	";
    	return $sql;
    }

    public function GetTableColumnsQuery($tablename){
    	$sql = "SELECT * FROM information_schema.columns WHERE table_schema = 
    		'".$this->DatabaseName."' AND table_name = '".$tablename."'";
    	return $sql;
    }

    public function GetEndPointHasProgressQuery($endpoint){
    	$sql = "SELECT * FROM ".$this->ProgressTableName." WHERE end_point = 
    		'".$endpoint."'";
    	return $sql;
    }


    public function GetCreateEndPointProgressQuery($endpoint){
    	$sql = "INSERT INTO ".$this->ProgressTableName." ( end_point, draft_id) VALUES 
    	 ('".$endpoint."', '0' ) ";
    	return $sql;
    }



    public function GetEndPointProgressQuery($endpoint){
    	$sql = "SELECT * FROM ".$this->ProgressTableName." WHERE end_point  = '".$endpoint."'
    	 LIMIT 1 ";
    	return $sql;
    }


    public function GetNextDraftsQuery($id){
    	$id = intval($id);
    	$sql = "SELECT * FROM ".$this->DraftTableName." WHERE id > ".$id."
    	 ORDER BY id ASC ";
    	return $sql;
    }

    public function GetLastDraftsQuery(){
    	$sql = "SELECT * FROM ".$this->DraftTableName." ORDER BY id DESC LIMIT 1 ";
    	return $sql;
    }


    public function GetNextDraftRecordQuery($tableName, $uniqueValue){
    	$uniqueColomnName = $this->GetTableUniqueColomnName($tableName);
    	$sql = "SELECT * FROM ".$tableName." WHERE ".$uniqueColomnName." = '".$uniqueValue."'";
    	return $sql;
    }

    public function GetUpdateProgressQuery($endpoint, $value){
    	$sql = "UPDATE ".$this->ProgressTableName." SET  draft_id = '".$value."', 
    	 date_created = NOW() WHERE end_point = '".$endpoint."'";
    	return $sql;
    }


	public function PushError($message){
		array_push($this->errors, $message);
	}

	public function HasErrors()
	{
		if(count($this->errors) > 0){
			return true;
		}
		return false;
	}

	public function GetTableUniqueColomnName($tablename){
		if(isset($this->TargetTables[$tablename])){
			return $this->TargetTables[$tablename];
		}
		$this->PushError("GetTableUniqueColomnName: Table " . $tablename. " does not exist");
		return '';
	}
}

$Settings = new Setup();
?>