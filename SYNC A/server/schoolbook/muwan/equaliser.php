<?php  
	//nyd
	//implement security access
	
	//database
	include("db_connection.php");

	//the utility 
	include("utility.php");

	//this expects branchid, table, value
	//the valuse is the last unique value
	if(strcasecmp($Settings->DataBaseType, "mysql") == 0){
		//if there are non branches involved 
		//we altleast expect this to be having a value of zero
		if(!isset($_GET["branchid"])){
			$Settings->PushError("branchid is required");
		}

		if(!isset($_GET["table"])){
			$Settings->PushError("table is required");
		}

		if(!isset($_GET["value"])){
			$Settings->PushError("value is required, this is the last synchronised unqiue value");
		}

		//proceed if there are no errors
		if($Settings->HasErrors() == false){

			//make progress entry for this end point
			//check if this endpoint has a progress record
			$sql = $Settings->GetEndPointHasProgressQuery($_GET["endpoint"]);
			$hasProgress = false;
			try{
				$res = $conn->query($sql);
				foreach ($res as $row ) {
					$hasProgress = true;
					break;
				}
				if($hasProgress == false){
					//create a new progress record for this endpoint
					$sql = $Settings->GetCreateEndPointProgressQuery($_GET["endpoint"]);
					$res = $conn->exec($sql);
				}	
				$draftProgress = 0;
				$sql = $Settings->GetLastDraftsQuery();
				//respond($sql);
				try{						
					$res = $conn->query($sql);
					foreach ($res as $row ) {
						$draftProgress = $row["id"];
						break;
					}
		    	}catch(PDOException $e)
		    	{
		    		$Settings->PushError($e->getMessage());
		    	}
				//update this progress with the current draft id
				$sql = $Settings->GetUpdateProgressQuery($_GET["endpoint"], $draftProgress);
				//respond($sql);
				try{						
					$res = $conn->exec($sql);
		    	}catch(PDOException $e)
		    	{
		    		$Settings->PushError($e->getMessage());
		    	}
				//respond("Check the staff out my friensd");
	    	}catch(PDOException $e)
	    	{
	    		$Settings->PushError($e->getMessage());
	    	}
	    	if($Settings->HasErrors()){
				respond(array());
			}




			$sql = $Settings->EqualisationQuery;
			//replace parameters
			$sql = str_ireplace("tablename", $_GET["table"], $sql);
			//get the tableUniqueColomnName
			$ColomnName = $Settings->GetTableUniqueColomnName($_GET["table"]);
			if($Settings->HasErrors()){
				respond(array());
			}
			$sql = str_ireplace("tableUniqueColomnName", $ColomnName, $sql);
			$sql = str_ireplace("lastUniqueValue", $_GET["value"], $sql);
			//check if this system is branched
			if($Settings->IsBranched == true){
				//check if this has a branch colomn
				if(isset($Settings->BranchConfig[$_GET["table"]])){
					$sql = $sql . " AND " . $Settings->BranchConfig[$_GET["table"]] . " = '" . $_GET["branchid"] . "' " ;
				}
			}

			try{
				$records = array();

				//respond($sql);
				//execute the sql
				$res = $conn->query($sql);

				foreach ($res as $row ) {
					$thisRow = array();
					foreach ($row as $key => $value) {						
						if(is_numeric($key) == false){
							$thisColumn = array(
								"Name" => $key,
								"Value" => $value
							);
							array_push($thisRow, $thisColumn);
			    		}
			    	}
			    	array_push($records, $thisRow );
				}

				respond($records);
			}catch(PDOException $e)
	    	{
	    		$Settings->PushError($e->getMessage());
	    	}
		}
	}else{
		$Settings->PushError("Database type " . $Settings->DataBaseType . " is not yet supported");
	}

	respond(array());
?>