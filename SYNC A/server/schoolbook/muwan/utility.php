<?php
	function respond($data){
		global $conn;
		global $Settings;

		$conn = null;

		if(!is_array($data)){
			$data = array(
				array(
					"Name" => "MuwanResponse",
					"Value" => $data
				)
			);
		}

		//The response
		$res= array(
			"Data" => $data,
			"Errors" => $Settings->errors
		);
		header("Content-type: application/json"); 
		echo json_encode($res);
		exit(0);
	}
?>