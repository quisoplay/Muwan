<?php
include("settings.php");
try {
    $conn = new PDO("mysql:host=$Settings->ServerName;dbname=$Settings->DatabaseName", $Settings->UserName, $Settings->Password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e)
{
	array_push($Settings->errors, $e->getMessage());
}
?>