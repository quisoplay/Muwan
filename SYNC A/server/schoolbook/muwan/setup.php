<?php 
	//nyd
	//implement security access
	
	//database
	include("db_connection.php");

	//the utility 
	include("utility.php");

	if(strcasecmp($Settings->DataBaseType, "mysql") == 0){
		//create draft table
        $sql = $Settings->GetCreateDraftTableQuery();
        try{
        	//execute the sql
			$res = $conn->exec($sql);
        }catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

		//create the progress table 
		//this one will be used by endpoints to rember their draft
		//progress
		$sql = $Settings->GetCreateProgressTableQuery();
        try{
        	//execute the sql
			$res = $conn->exec($sql);
        }catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

		//create the claims table 
		//this one will be used by endpoints to rember their draft
		//entries and to avoid repeations
		$sql = $Settings->GetCreateClaimTableQuery();
        try{
        	//execute the sql
			$res = $conn->exec($sql);
        }catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

		//create table triggers on target tables
		//Insert triggers
		try{
			foreach ($Settings->TargetTables as $tablename => $uniqueColomnName) {						
				$sql = $Settings->GetInsertTrigerQuery($tablename);
				//execute the sql
				$res = $conn->exec($sql);
	    	}
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}

    	//update triggers
		try{
			foreach ($Settings->TargetTables as $tablename => $uniqueColomnName) {						
				$sql = $Settings->GetUpdateTrigerQuery($tablename);
				//execute the sql
				$res = $conn->exec($sql);
	    	}
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}

    	//delete triggers
		try{
			foreach ($Settings->TargetTables as $tablename => $uniqueColomnName) {						
				$sql = $Settings->GetDeleteTrigerQuery($tablename);
				//execute the sql
				$res = $conn->exec($sql);
	    	}
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}

	}else{
		$Settings->PushError("Database type " . $Settings->DataBaseType . " is not yet supported");
	}
?>