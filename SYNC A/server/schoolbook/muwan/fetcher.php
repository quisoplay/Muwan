<?php  
	//nyd
	//implement security access
	
	//database
	include("db_connection.php");

	//the utility 
	include("utility.php");

	//this expects branchid, endpoint
	if(strcasecmp($Settings->DataBaseType, "mysql") == 0){
		//check the branchid
		if(!isset($_GET["branchid"])){
			$Settings->PushError("branchid is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check the endpoint id is required
		if(!isset($_GET["endpoint"])){
			$Settings->PushError("endpoint is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check if this endpoint has a progress record
		$sql = $Settings->GetEndPointHasProgressQuery($_GET["endpoint"]);
		$hasProgress = false;
		try{
			$res = $conn->query($sql);
			foreach ($res as $row ) {
				$hasProgress = true;
				break;
			}
			if($hasProgress == false){
				//create a new progress record for this endpoint
				$sql = $Settings->GetCreateEndPointProgressQuery($_GET["endpoint"]);
				$res = $conn->exec($sql);
			}
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

    	//fetch the last progress value for this endpoint
    	$sql = $Settings->GetEndPointProgressQuery($_GET["endpoint"]);
    	$lastProgress = "0";
    	try{
			$res = $conn->query($sql);
			$hadData = false;
			foreach ($res as $row ) {
				$lastProgress = $row["draft_id"];
				$hadData = true;
				break;
			}
			if($hadData == false){ //this is an abomiantion
				$Settings->PushError("Failed to get endpoint progress");
			}
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

		//get the next draft record
		$sql = $Settings->GetNextDraftsQuery($lastProgress);
		$thisDraft = null;
		try{
			$res = $conn->query($sql);
			foreach ($res as $row ) {
				$thisDraft = array();
				$thisDraft["id"] = $row["id"];
				$thisDraft["affected_table"] = $row["affected_table"];
				$thisDraft["activity"] = $row["activity"];
				$thisDraft["unique_value"] = $row["unique_value"];
				$thisDraft["date_created"] = $row["date_created"];
				//check that this draft is not already claimed by the endpoint
				$sql = $Settings->GetClaimQuery(
					$_GET["endpoint"],
					$thisDraft["affected_table"],
					$thisDraft["activity"],
					$thisDraft["unique_value"]
				);
				$wasClaimed = false;
				$resClaim = $conn->query($sql);
				foreach ($resClaim as $row ) {
					$wasClaimed = true;
					break;
				}
				if($wasClaimed){
					continue;
				}

				if(strcasecmp($thisDraft["activity"], "input") == 0 || 
				   strcasecmp($thisDraft["activity"], "modify") == 0){
					$sql = $Settings->GetNextDraftRecordQuery($thisDraft["affected_table"], $thisDraft["unique_value"]);
					//for each draft row get a target table record which belongs to 
					//this branch id, that id if the system is branched
					//and the target table also has a branch id column
					if($Settings->IsBranched){
						
						//check if this has a branch colomn
						if(isset($Settings->BranchConfig[$thisDraft["affected_table"]])){
							$sql = $sql . " AND " . $Settings->BranchConfig[$thisDraft["affected_table"]] . " = '" . $_GET["branchid"] . "' " ;
						}

					}

					//fetch the data related to this draft
					$records = array();
					$hadData = false;
					try{
						$res2 = $conn->query($sql);
						
						foreach ($res2 as $row2 ) {
							$hadData = true;
							$thisRow = array();
							foreach ($row2 as $key => $value) {						
								if(is_numeric($key) == false){
									$thisColumn = array(
										"Name" => $key,
										"Value" => $value
									);
									array_push($thisRow, $thisColumn);
					    		}
					    	}
					    	//add the necessary info for this record
							//muwan_table, muwan_activity, muwan_draft_id
							$thisColumn = array(
								"Name" => "muwan_draft_id" ,
								"Value" => $thisDraft["id"]
							);		
							array_push($thisRow, $thisColumn);				
							$thisColumn = array(
								"Name" => "muwan_table",
								"Value" => $thisDraft["affected_table"]
							);
							array_push($thisRow, $thisColumn);
							$thisColumn = array(
								"Name" => "muwan_activity",
								"Value" => $thisDraft["activity"]
							);
							array_push($thisRow, $thisColumn);
					    	array_push($records, $thisRow );
					    	break;
						}
						
			    	}catch(PDOException $e)
			    	{
			    		$Settings->PushError($e->getMessage());
			    	}
			    	if($Settings->HasErrors()){
						respond(array());
					}
					if($hadData == false){ 
					    //this is my happen if the data was deleted
					    //so again we find the tally hole here
						//$Settings->PushError("Failed to get data for the draft row");
						//and also this branch may just have no data, so i dont know what to do
						continue;
					}
					respond($records);					
				}elseif(strcasecmp($thisDraft["activity"], "remove") == 0){
					$uniqueColomnName = $Settings->GetTableUniqueColomnName($thisDraft["affected_table"]);

					$records = array();

					$thisRow = array();	
					$thisColumn = array(
						"Name" => "muwan_draft_id" ,
						"Value" => $thisDraft["id"]
					);
					array_push($thisRow, $thisColumn);	
					$thisColumn = array(
						"Name" => $uniqueColomnName ,
						"Value" => $thisDraft["unique_value"]
					);
					array_push($thisRow, $thisColumn);						
					$thisColumn = array(
						"Name" => "muwan_table",
						"Value" => $thisDraft["affected_table"]
					);
					array_push($thisRow, $thisColumn);
					$thisColumn = array(
						"Name" => "muwan_activity",
						"Value" => $thisDraft["activity"]
					);
					array_push($thisRow, $thisColumn);

			    	array_push($records, $thisRow );

			    	respond($records);
				}else{
					$Settings->PushError("Unknown activity in draft row: " + $thisDraft["activity"] );
				}
			}
			//well send an empty indicator to let the 
			//end point no that there was no data
			respond(array());
    	}catch(PDOException $e)
    	{
    		$Settings->PushError($e->getMessage());
    	}
    	if($Settings->HasErrors()){
			respond(array());
		}

	}else{
		$Settings->PushError("Database type " . $Settings->DataBaseType . " is not yet supported");
	}

	respond(array());
?>