<?php  
	//nyd
	//implement security access
	
	//database
	include("db_connection.php");

	//the utility 
	include("utility.php");

	//this expects muwan_branchid, muwan_table, muwan_activity, endpoint
	//the valuse must not be one of these
	if(strcasecmp($Settings->DataBaseType, "mysql") == 0){
		//check the activity
		if(!isset($_POST["muwan_activity"])){
			$Settings->PushError("muwan_activity is required, should be one of standard activities");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check the branch id is required
		if(!isset($_POST["muwan_branchid"])){
			$Settings->PushError("muwan_branchid is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check the table
		if(!isset($_POST["muwan_table"])){
			$Settings->PushError("muwan_table is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//check the endpoint
		if(!isset($_POST["endpoint"])){
			$Settings->PushError("endpoint is required");
		}
		if($Settings->HasErrors()){
			respond(array());
		}

		//execute according to the actionspecified
		if(strcasecmp($_POST["muwan_activity"], $Settings->TallyActivities["input"]) == 0 ){
			//get this tables columns
			$sql = $Settings->GetTableColumnsQuery($_POST["muwan_table"]);
			//respond($sql);
			try{
				$newSql = "INSERT INTO " . $_POST["muwan_table"] . " (";
				$cols = "";
				$values = "";
				$res = $conn->query($sql);
				$uniqueColumnName = $Settings->GetTableUniqueColomnName($_POST["muwan_table"]);
				//respond($uniqueColumnName );
				foreach ($res as $row ) {
					foreach ($row as $key => $value) {						
						if(is_numeric($key) == false && isset($_POST[$value])){
							//if the database type is auto skip the id thing
							if(strcasecmp($Settings->IdType, "auto") == 0 &&
							   strcasecmp($value, $uniqueColumnName) == 0 ){
								continue;
							}
							$cols = $cols . $value . ", ";							
							$values = $values . "'" .$_POST[$value]."', ";
						    break;
						}
					}
		    	}
		    	//remove trailing commas
		    	$colsx = substr($cols,0,strlen($cols)-2); //rtrim($cols, ',');
		    	$valuesx = substr($values,0,strlen($values)-2); //rtrim($values, ',');

		    	$newSql = $newSql . $colsx . " ) VALUES ( " . $valuesx . " )";
				//respond($newSql);
				$conn->exec($newSql);

				$newId = $conn->lastInsertId();

				//make a claim for this endpoint
				$sql = $Settings->GetInsertClaimQuery(
					$_POST["endpoint"], 
					$_POST["muwan_table"], 
					"input", 
					$newId
				);
				$conn->exec($sql);

				$records = array();
				$thisRow = array();
				$thisColumn = array(
					"Name" => "ForeignKey",
					"Value" => $newId
				);
				array_push($thisRow, $thisColumn);
				array_push($records, $thisRow );

				respond($records);

	    	}catch(PDOException $e)
	    	{
	    		$Settings->PushError($e->getMessage());
	    	}
		}else if(strcasecmp($_POST["muwan_activity"], $Settings->TallyActivities["modify"]) == 0 ){
			//get this tables columns
			$sql = $Settings->GetTableColumnsQuery($_POST["muwan_table"]);
			//respond($sql);
			try{
				$newSql = "UPDATE  " . $_POST["muwan_table"] . " SET ";
				$cols = "";
				$valuex = "";
				$res = $conn->query($sql);
				$uniqueColumnName = $Settings->GetTableUniqueColomnName($_POST["muwan_table"]);
				//respond($uniqueColumnName );
				foreach ($_POST as $key => $value ) {						
					if(strcasecmp($key, "muwan_table") == 0 ||
					   strcasecmp($key, "muwan_activity") == 0 ||
					   strcasecmp($key, "muwan_branchid") == 0 ||
					   strcasecmp($key, "endpoint") == 0  ){
						continue;
					}
					//if the database type is auto skip the id thing
					if(strcasecmp($Settings->IdType, "auto") == 0 &&
					   strcasecmp($key, $uniqueColumnName) == 0 ){
					   	$valuex = $value;
						continue;
					}
					$cols = $cols . $key . " = '".$value."', ";		
		    	}
		    	//remove trailing commas
		    	$colsx = substr($cols,0,strlen($cols)-2); //rtrim($cols, ',');

		    	$newSql = $newSql . $colsx . " WHERE ".$uniqueColumnName." = '".$valuex."'";
				//respond($newSql);
				$conn->exec($newSql);

				//make a claim for this endpoint
				$sql = $Settings->GetInsertClaimQuery(
					$_POST["endpoint"], 
					$_POST["muwan_table"], 
					"modify", 
					$valuex
				);
				$conn->exec($sql);
				

				$records = array();
				$thisRow = array();
				$thisColumn = array(
					"Name" => "Status",
					"Value" => "ok"
				);
				array_push($thisRow, $thisColumn);
				array_push($records, $thisRow );

				respond($records);

	    	}catch(PDOException $e)
	    	{
	    		$Settings->PushError($e->getMessage());
	    	}
		}else if(strcasecmp($_POST["muwan_activity"], $Settings->TallyActivities["remove"]) == 0 ){
			$uniqueColumnName = $Settings->GetTableUniqueColomnName($_POST["muwan_table"]);
			$valuex = $_POST[$uniqueColumnName];
			$sql = "DELETE FROM ".$_POST["muwan_table"]." WHERE ".$uniqueColumnName." = '".$valuex."'";
			try{
				$res = $conn->exec($sql);

				//make a claim for this endpoint
				$sql = $Settings->GetInsertClaimQuery(
					$_POST["endpoint"], 
					$_POST["muwan_table"], 
					"remove", 
					$valuex
				);
				$conn->exec($sql);

				$records = array();

				$thisRow = array();
				$thisColumn = array(
					"Name" => "delete_results",
					"Value" => "ok"
				);
				array_push($thisRow, $thisColumn);

		    	array_push($records, $thisRow );

		    	respond($records);
			}catch(PDOException $e)
	    	{
	    		$Settings->PushError($e->getMessage());
	    	}
		}



	}else{
		$Settings->PushError("Database type " . $Settings->DataBaseType . " is not yet supported");
	}

	respond(array());
?>