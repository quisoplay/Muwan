﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class Adapted
    {
        public int id { get; set; }
        public string table_name { get; set; }       
        public string local_table_unique_value { get; set; }
        public string foreign_table_unique_value { get; set; }
        public string local_deleted { get; set; }
        public string foreign_deleted { get; set; }
        public string date_created { get; set; }
    }
}
