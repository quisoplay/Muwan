﻿using Newtonsoft.Json;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class Network
    {
        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string Post(string url){
            
            return "";
        }

        public void  test(){
            //string info = "==>Getting Branch Records For Table " + tableName + ", last unique value" + lastUniqueValue;
            //logger.Info(info);
            //string branchId = Convert.ToString( Setting.GetInstance().BranchId );
            //info = "the branch id " + branchId;
            //logger.Info(info);
            string url = Setting.GetInstance().BaseDomain;
            //info = "the base domain is " + url;
            //logger.Info(info);
            var client = new RestClient(url);
            string equaliserUrl = Setting.GetInstance().EqualiserURL;
            //info = "the equaliser url is " + equaliserUrl;
            //logger.Info(info);
            var request = new RestRequest(equaliserUrl, Method.POST);
            request.AddParameter("branchid", 23); // adds to POST or URL querystring based on Method
            //request.AddUrlSegment("branchid", branchId); // replaces matching token in request.Resource
            //request.AddUrlSegment("table", tableName); // replaces matching token in request.Resource
            //request.AddUrlSegment("value", lastUniqueValue); // replaces matching token in request.Resource
            ////easily add HTTP Headers
            //request.AddHeader("header", "value");
            ////add files to upload (works with compatible verbs)
            //request.AddFile(path);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            //info = "request has returned " + content;
            //logger.Info(info);
            
        }

        public string GetBranchRecordsForTable(String tableName, string lastUniqueValue)
        {
            string info = "==>Getting Branch Records For Table " + tableName + ", last unique value" + lastUniqueValue;
            logger.Info(info);
            string branchId = Convert.ToString(Setting.GetInstance().BranchId);
            info = "the branch id " + branchId;
            string endpoint = Convert.ToString(Setting.GetInstance().EndPoint);
            info = "the endpoint " + endpoint;
            logger.Info(info);
            string url = Setting.GetInstance().BaseDomain;
            info = "the base domain is " + url;
            logger.Info(info);
            var client = new RestClient(url);
            string equaliserUrl = Setting.GetInstance().EqualiserURL;
            info = "the equaliser url is " + equaliserUrl;
            logger.Info(info);
            var request = new RestRequest(equaliserUrl, Method.GET);
            request.AddParameter("branchid", branchId); // adds to POST or URL querystring based on Method
            request.AddParameter("endpoint", endpoint);
            request.AddParameter("table", tableName);
            request.AddParameter("value", lastUniqueValue);
            //request.AddUrlSegment("branchid", branchId); // replaces matching token in request.Resource
            //request.AddUrlSegment("table", tableName); // replaces matching token in request.Resource
            //request.AddUrlSegment("value", lastUniqueValue); // replaces matching token in request.Resource
            ////easily add HTTP Headers
            //request.AddHeader("header", "value");
            ////add files to upload (works with compatible verbs)
            //request.AddFile(path);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            info = "request has returned " + content;
            logger.Info(info);
            return content;

            //string foo = "[[{\"Name\":\"id\",\"Value\":1},{\"Name\":\"class_room_id\",\"Value\":\"1\"},{\"Name\":\"academic_year_id\",\"Value\":1},{\"Name\":\"name\",\"Value\":\"p1\"},{\"Name\":\"other\",\"Value\":null}]";
            //foo = foo + ",[{\"Name\":\"id\",\"Value\":2},{\"Name\":\"class_room_id\",\"Value\":\"1\"},{\"Name\":\"academic_year_id\",\"Value\":1},{\"Name\":\"name\",\"Value\":\"p2\"},{\"Name\":\"other\",\"Value\":null}]]";

            //foo = "[[{\"Name\":\"id\",\"Value\":1},{\"Name\":\"year\",\"Value\":\"2015\"},{\"Name\":\"start_date\",\"Value\":\"1454772119\"},{\"Name\":\"end_date\",\"Value\":\"1454772119\"}]";
            //foo = foo + ", [{\"Name\":\"id\",\"Value\":2},{\"Name\":\"year\",\"Value\":\"2016\"},{\"Name\":\"start_date\",\"Value\":\"1454772119\"},{\"Name\":\"end_date\",\"Value\":\"1454772119\"}]]";
            //return foo;
        }

        public string RowToJsonString(Column[] columns)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            JsonWriter jsonWriter = new JsonTextWriter(sw);

            try
            {

                int fieldcount = columns.Length; // count how many columns are in the row

                jsonWriter.WriteStartObject();
                for (int index = 0; index < fieldcount; index++)
                {   // iterate through all columns

                    jsonWriter.WritePropertyName(columns[index].Name); // column name
                    jsonWriter.WriteValue(columns[index].Value); // value in column

                }


                jsonWriter.WriteEndObject();

            }
            catch (Exception ex)
            { // exception
                String exstr = "Exception in string generation \n";
                logger.Info(exstr);
            }

            string jsonRow = sb.ToString();

            return jsonRow;
        }
        
        public string SendBranchRecordForTable(String tableName, Column[] row, Setting.TallyActivities activity)
        {
            string info = "==>Sending a Branch Record For Table " + tableName ;
            logger.Info(info);
            string branchId = Convert.ToString(Setting.GetInstance().BranchId);
            info = "the branch id " + branchId;
            string endpoint = Setting.GetInstance().EndPoint;
            info = "the endpoint is " + endpoint;
            logger.Info(info);
            string url = Setting.GetInstance().BaseDomain;
            info = "the base domain is " + url;
            logger.Info(info);
            var client = new RestClient(url);
            string persisterUrl = Setting.GetInstance().PersisterURL;
            info = "the persister url is " + persisterUrl;
            logger.Info(info);
            var request = new RestRequest(persisterUrl, Method.POST);
            request.AddParameter("muwan_branchid", branchId); // adds to POST or URL querystring based on Method
            request.AddParameter("muwan_table", tableName);
            request.AddParameter("endpoint", endpoint);

            if (activity == Setting.TallyActivities.input)
            {
                request.AddParameter("muwan_activity", Convert.ToString( Setting.TallyActivities.input) );
            }

            if (activity == Setting.TallyActivities.modify)
            {
                request.AddParameter("muwan_activity", Convert.ToString(Setting.TallyActivities.modify));               
            }

            if (activity == Setting.TallyActivities.remove)
            {
                request.AddParameter("muwan_activity", Convert.ToString(Setting.TallyActivities.remove));
            }

            //this when the record is converted to a json string
            //string json =  RowToJsonString(row);
            //request.AddParameter("data", json);
            for (int i = 0; i < row.Length; i++)
            {
                request.AddParameter(row[i].Name, row[i].Value);
            }


            //request.AddUrlSegment("branchid", branchId); // replaces matching token in request.Resource
            //request.AddUrlSegment("table", tableName); // replaces matching token in request.Resource
            //request.AddUrlSegment("value", lastUniqueValue); // replaces matching token in request.Resource
            ////easily add HTTP Headers
            //request.AddHeader("header", "value");
            ////add files to upload (works with compatible verbs)
            //request.AddFile(path);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            info = "request has returned " + content;
            logger.Info(info);
            return content;
        }

        public string FetchNextDraft()
        {
            string info = "==>Getting the next draft record from the server";
            logger.Info(info);
            string branchId = Convert.ToString(Setting.GetInstance().BranchId);
            info = "the branch id " + branchId;
            string endPoint = Convert.ToString(Setting.GetInstance().EndPoint);
            info = "the endpoint is " + endPoint;
            logger.Info(info);
            string url = Setting.GetInstance().BaseDomain;
            info = "the base domain is " + url;
            logger.Info(info);
            var client = new RestClient(url);
            string fetcherUrl = Setting.GetInstance().FetcherURL;
            info = "the fetcher url is " + fetcherUrl;
            logger.Info(info);
            var request = new RestRequest(fetcherUrl, Method.GET);
            request.AddParameter("branchid", branchId); // adds to POST or URL querystring based on Method
            request.AddParameter("endpoint", endPoint);
            
            //request.AddUrlSegment("branchid", branchId); // replaces matching token in request.Resource
            //request.AddUrlSegment("table", tableName); // replaces matching token in request.Resource
            //request.AddUrlSegment("value", lastUniqueValue); // replaces matching token in request.Resource
            ////easily add HTTP Headers
            //request.AddHeader("header", "value");
            ////add files to upload (works with compatible verbs)
            //request.AddFile(path);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            info = "request has returned " + content;
            logger.Info(info);
            return content;

            //string foo = "[[{\"Name\":\"id\",\"Value\":1},{\"Name\":\"class_room_id\",\"Value\":\"1\"},{\"Name\":\"academic_year_id\",\"Value\":1},{\"Name\":\"name\",\"Value\":\"p1\"},{\"Name\":\"other\",\"Value\":null}]";
            //foo = foo + ",[{\"Name\":\"id\",\"Value\":2},{\"Name\":\"class_room_id\",\"Value\":\"1\"},{\"Name\":\"academic_year_id\",\"Value\":1},{\"Name\":\"name\",\"Value\":\"p2\"},{\"Name\":\"other\",\"Value\":null}]]";

            //foo = "[[{\"Name\":\"id\",\"Value\":1},{\"Name\":\"year\",\"Value\":\"2015\"},{\"Name\":\"start_date\",\"Value\":\"1454772119\"},{\"Name\":\"end_date\",\"Value\":\"1454772119\"}]";
            //foo = foo + ", [{\"Name\":\"id\",\"Value\":2},{\"Name\":\"year\",\"Value\":\"2016\"},{\"Name\":\"start_date\",\"Value\":\"1454772119\"},{\"Name\":\"end_date\",\"Value\":\"1454772119\"}]]";
            //return foo;
        }

        public string MakeProgress(string lastDraftId)
        {
            string info = "==>Making  progress " + lastDraftId;
            logger.Info(info);
            string endpoint = Setting.GetInstance().EndPoint;
            info = "the endpoint is " + endpoint;
            logger.Info(info);
            string url = Setting.GetInstance().BaseDomain;
            info = "the base domain is " + url;
            logger.Info(info);
            var client = new RestClient(url);
            string progrsserURL = Setting.GetInstance().ProgresserURL;
            info = "the progresser url is " + progrsserURL;
            logger.Info(info);
            var request = new RestRequest(progrsserURL, Method.POST);
            request.AddParameter("endpoint", endpoint); // adds to POST or URL querystring based on Method
            request.AddParameter("lastDraftId", lastDraftId);

            //request.AddUrlSegment("branchid", branchId); // replaces matching token in request.Resource
            //request.AddUrlSegment("table", tableName); // replaces matching token in request.Resource
            //request.AddUrlSegment("value", lastUniqueValue); // replaces matching token in request.Resource
            ////easily add HTTP Headers
            //request.AddHeader("header", "value");
            ////add files to upload (works with compatible verbs)
            //request.AddFile(path);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            info = "request has returned " + content;
            logger.Info(info);
            return content;
        }
        
        
        public void work()
        {
            var client = new RestClient("http://example.com");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("resource/{id}", Method.POST);
            request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
            request.AddUrlSegment("id", "123"); // replaces matching token in request.Resource

            // easily add HTTP Headers
            request.AddHeader("header", "value");

            // add files to upload (works with compatible verbs)
            //request.AddFile(path);

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string

            // or automatically deserialize result
            // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
            //RestResponse<Person> response2 = client.Execute<Person>(request);
            //var name = response2.Data.Name;

            // easy async support
            //client.ExecuteAsync(request, response =>
            //{
            //    Console.WriteLine(response.Content);
            //});

            // async with deserialization
            //var asyncHandle = client.ExecuteAsync<Person>(request, response =>
            //{
            //    Console.WriteLine(response.Data.Name);
            //});

            // abort the request on demand
           // asyncHandle.Abort();


        }


    }
}
