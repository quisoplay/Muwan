﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class Setting 
    {

        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public bool IsBranched { get { return this.SettingsModel.IsBranched; } }
        public int BranchId { get { return this.SettingsModel.BranchId; } }
        public string EndPoint { get { return this.SettingsModel.EndPoint; } }

        public string DatabaseUser { get { return this.SettingsModel.DatabaseUser; } }

        public string CleanDatabaseUser { get { return this.SettingsModel.CleanDatabaseUser; } }

        public long TallyWatcherInterval { get { return this.SettingsModel.TallyWatcherInterval; } }
        public long DraftWatcherInterval { get { return this.SettingsModel.DraftWatcherInterval; } }

        public string BaseDomain { get { return this.SettingsModel.BaseDomain; } }
        public string PersisterURL { get { return this.SettingsModel.PersisterURL; } }
        public string FetcherURL { get { return this.SettingsModel.FetcherURL; } }
        public string EqualiserURL { get { return this.SettingsModel.EqualiserURL; } }
        public string ProgresserURL { get { return this.SettingsModel.ProgresserURL; } }


        public string DataBaseType { get { return this.SettingsModel.DataBaseType; } }
        public string ConnectionString { get { return this.SettingsModel.ConnectionString; } }
        public string TallyTableName { get { return this.SettingsModel.TallyTableName; } }
        public string AdapterTableName { get { return this.SettingsModel.AdapterTableName; } }
        public string EqualiseTableName { get { return this.SettingsModel.EqualiseTableName; } }
       
        public bool SkipMissingRecord { get { return this.SettingsModel.SkipMissingRecord; } }



        public string CreateTallyTableQuery { 
            get {
                string tableName = this.SettingsModel.TallyTableName;
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CreateTallyTableQuery))
                {
                    query = this.SettingsModel.CreateTallyTableQuery.Replace("tallyTableName", TallyTableName);
                }
                return query;
            } 
        }

        public string CreateAdapterTableQuery
        {
            get
            {
                string tableName = this.SettingsModel.AdapterTableName;
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CreateAdapterTableQuery))
                {
                    query = this.SettingsModel.CreateAdapterTableQuery.Replace("adapterTableName", AdapterTableName);
                }
                return query;
            }
        }

        public string CreateEqualiseTableQuery
        {
            get
            {
                string tableName = this.SettingsModel.EqualiseTableName;
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CreateEqualiseTableQuery))
                {
                    query = this.SettingsModel.CreateEqualiseTableQuery.Replace("equaliseTableName", EqualiseTableName);
                }
                return query;
            }
        }

        public List<Target> TargetTables { get { return this.SettingsModel.TargetTables; } }
        public string InsertTrigerQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.InsertTrigerQuery))
                {
                    query = this.SettingsModel.InsertTrigerQuery.Replace("dataBaseUser", CleanDatabaseUser);
                }
                if (!String.IsNullOrEmpty(query))
                {
                    query = query.Replace("tallyTableName", TallyTableName);
                }
                return query;
            }
        }

        public string UpdateTrigerQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.UpdateTrigerQuery))
                {
                    query = this.SettingsModel.UpdateTrigerQuery.Replace("dataBaseUser", CleanDatabaseUser);
                }
                if (!String.IsNullOrEmpty(query))
                {
                    query = query.Replace("tallyTableName", TallyTableName);
                }
                return query;
            }
        }
        public string DeleteTrigerQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.DeleteTrigerQuery))
                {
                    query = this.SettingsModel.DeleteTrigerQuery.Replace("dataBaseUser", CleanDatabaseUser);
                }
                if (!String.IsNullOrEmpty(query))
                {
                    query = query.Replace("tallyTableName", TallyTableName);
                }
                return query;
            }
        }

        public string NextTallyQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.NextTallyQuery))
                {
                    query = this.SettingsModel.NextTallyQuery.Replace("tallyTableName", TallyTableName);
                }
               
                return query;
            }
        }

        public string TallyFetchQuery { get { return this.SettingsModel.TallyFetchQuery; } }

        public string CheckEqualisedTargetQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CheckEqualisedTargetQuery))
                {
                    query = this.SettingsModel.CheckEqualisedTargetQuery.Replace("equaliseTableName", EqualiseTableName);
                }

                return query;
            }
        }

        public string InsertEqualisationTargetQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.InsertEqualisationTargetQuery))
                {
                    query = this.SettingsModel.InsertEqualisationTargetQuery.Replace("equaliseTableName", EqualiseTableName);
                }

                return query;
            }
        }

        public string CheckAdaptedQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CheckAdaptedQuery))
                {
                    query = this.SettingsModel.CheckAdaptedQuery.Replace("adapterTableName", AdapterTableName);
                }

                return query;
            }
        }

        public string PersistNewRecordQuery { get { return this.SettingsModel.PersistNewRecordQuery; } }

        public string UpdateRecordQuery { get { return this.SettingsModel.UpdateRecordQuery; } }

        public string InsertAdaptationQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.InsertAdaptationQuery))
                {
                    query = this.SettingsModel.InsertAdaptationQuery.Replace("adapterTableName", AdapterTableName);
                }

                return query;
            }
        }

        public string DeleteRecordQuery { get { return this.SettingsModel.DeleteRecordQuery; } }

        public string UpdateEqualisedTargetQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.UpdateEqualisedTargetQuery))
                {
                    query = this.SettingsModel.UpdateEqualisedTargetQuery.Replace("equaliseTableName", EqualiseTableName);
                }

                return query;
            }
        }

        public string CheckPersistedRecordQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.CheckPersistedRecordQuery))
                {
                    query = this.SettingsModel.CheckPersistedRecordQuery.Replace("adapterTableName", AdapterTableName);
                }

                return query;
            }
        }

        public string UpdateAdaptedQuery
        {
            get
            {
                string query = "";
                if (!String.IsNullOrEmpty(this.SettingsModel.UpdateAdaptedQuery))
                {
                    query = this.SettingsModel.UpdateAdaptedQuery.Replace("adapterTableName", AdapterTableName);
                }

                return query;
            }
        }
       
        /// <summary>
        /// The id type is important because the if it is guid
        /// then there is no need for the adapter table and there for
        /// no need for the muwan translation step
        /// </summary>
        public IdTypes IdType { 
            get { 
                if(string.IsNullOrEmpty(this.SettingsModel.IdType)){
                    return IdTypes.auto;
                }

                if (this.SettingsModel.IdType.Equals(Convert.ToString(Setting.IdTypes.auto)))
                {
                    return IdTypes.auto;
                }

                if(this.SettingsModel.IdType.Equals( Convert.ToString(Setting.IdTypes.guid) )){
                    return IdTypes.guid;
                }

                return IdTypes.auto;
            } 
        }

        public enum DataBaseTypes
        {
            mssql = 1,
            mysql = 2,
            mysqli = 3,
            psql = 4,
            oracle = 5
        };

        public enum IdTypes
        {
            auto = 1,
            guid = 2
        };

        public enum TallyActivities{
            input = 1,
            modify = 2,
            remove = 3
        }

        public const string StopErrorMessage = "\nProgram experienced an error earlier, so execution has been stopped";

        //atomic parttern
        private static Setting Instance;

        public static Setting GetInstance (){            
            if (Setting.Instance == null)
            {
                Setting.Instance = new Setting();
            }
            return Setting.Instance;           
        }

        public static void DisposeInstance()
        {
            Setting.Instance = null;          
        }

        private SettingsModel SettingsModel;

        private Setting()
        {
            this.SettingsModel = new SettingsModel();
            logger.Info("==>Created the settings object");
        }

        public void LoadSettings()
        {
            logger.Info("==>Loading settings");

            string fileName = "setting.json";
            string currentDirectory = System.Environment.CurrentDirectory;
            string filePath = Path.Combine(currentDirectory, fileName);
            string i = "Path for settings is : " + filePath;
            logger.Info(i);
            //read in the json file
            if(File.Exists(filePath)){
                FileStream file = File.Open(filePath, FileMode.Open);
                StreamReader sr = new StreamReader(file);
                string settingString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                i = "The follwing settings were found : " + settingString;
                logger.Info(i);
                SettingsModel model =  JsonConvert.DeserializeObject<SettingsModel>(settingString);
                this.SettingsModel = model;
                logger.Info("Settings loaded successfully");
                logger.Info("Validating settings");
                this.validate();
            }else{
                logger.Info("Settings file was not found");
                string rx = "The settings file was not found at :" + filePath;
                logger.Info(rx);
                throw new Exception(rx);
            }
        }

        public void validate()
        {
            //check if there are any target tables 
            if (this.SettingsModel.TargetTables == null || this.SettingsModel.TargetTables.Count() == 0)
            {
                logger.Info("Exception in settings, There are not tables to synchronise");
                throw new Exception("Exception in setup, There are not tables to synchronise");
            }

            List<string> targetNames = new List<string>();
            foreach (Target target in this.SettingsModel.TargetTables)
            {
                targetNames.Add(target.TableName);
            }

            //check that these targets are fine
            foreach (Target target in this.SettingsModel.TargetTables)
            {
                //a target must have a name and a unique colum name
                if (string.IsNullOrEmpty(target.TableName))
                {
                    string info = "Exception in settings, One of the Target table names is not specified";
                    logger.Info(info);
                    throw new Exception("Exception in setup, One of the Target table names is not specified");
                }

                if (string.IsNullOrEmpty(target.UniqueColumnName))
                {
                    string info = "Exception in settings, no unique column name has been supplied for target " + target.TableName;
                    logger.Info(info);
                    throw new Exception(info);
                }

                //if there are any foreign keys, then they must be tables that are also going to be synchronised
                //this is important for the adapter and the translation step of the muwan service
                if (target.ForeignKeys != null && target.ForeignKeys.Count() > 0)
                {
                    foreach (Parent parent in target.ForeignKeys)
                    {
                        //this parent key must have a column name
                        if (string.IsNullOrEmpty(parent.ColumnName))
                        {
                            string info = "Exception in settings, no column name has been supplied for parent of  " + target.TableName;
                            logger.Info(info);
                            throw new Exception(info);
                        }
                        //the table name of the parent must also be listed for synchronisation
                        if (!targetNames.Contains(parent.TableName))
                        {
                            string info = "Exception in settings, the parent table specified for this foreign key " +
                                           parent.ColumnName + " of target " + target.TableName + " is not listed for synchronisation" ;
                            logger.Info(info);
                            throw new Exception(info);
                        }
                    }
                }
            }

            //the database user must be specified
            if (string.IsNullOrEmpty(this.SettingsModel.DatabaseUser))
            {
                string info = "Exception in settings, the muwan database user has not been specified  ";
                logger.Info(info);
                throw new Exception(info);
            }

            //validate the id type has been set
            if (string.IsNullOrEmpty(this.SettingsModel.IdType))
            {
                string info = "Exception in settings, the id type is not specified ";
                logger.Info(info);
                throw new Exception(info);
            }
        }

        public static string GetCorrectedTallyFetchQuery(String tableName, string tableUniqueColomnName, string uniqueValue)
        {
            string query = Setting.GetInstance().TallyFetchQuery;
            query = query.Replace("tableName", tableName);
            query = query.Replace("tableUniqueColomnName", tableUniqueColomnName);
            query = query.Replace("uniqueValue", uniqueValue);
            return query;
        }

        public Target GetTargetTable(String tableName)
        {
            Target target = TargetTables.FirstOrDefault(t => t.TableName == tableName);
            return target;
        }

        /// <summary>
        ///     Its important to sort the targets because other functionality will depend on the 
        ///     order by which data occurs, that is a child record cannot be inserted before the parent record
        /// </summary>
        public void SortTargets()
        {
            logger.Info("==>Sorting targets action \n");
            List<string> que = new List<string>();
            //make a copy of the targets
            List<Target> TargetsCopy = new List<Target>();
            foreach (Target target in TargetTables)
	        {
		        if(!TargetsCopy.Contains(target)){
                    TargetsCopy.Add(target);
                }
	        }
            //verify that they are of the same size
            if(TargetsCopy.Count != TargetTables.Count){
                throw  new Exception("Failed to sort targets due to ");
            }

            //the trick is that the targets must be moved from the copy target list to the que
            while(que.Count != TargetTables.Count){
                //each target is registered with its parents
                //generally every parent must come before its child
                for(int i=0; i < TargetsCopy.Count; i++)
                {
                    //get the target target
                    Target thisTarget = TargetsCopy[i];
                    //if it has no foreign keys then it is put in the cue
                    if (thisTarget.ForeignKeys == null || thisTarget.ForeignKeys.Count == 0)
                    {
                        //put this target in the que
                        que.Add(thisTarget.TableName);
                        //remove this target from the targets copy list
                        TargetsCopy.Remove(thisTarget);
                        //break
                        break;
                    }
                     
                    //or if all its foreign keys are in the que then its also put in the que
                    bool allThere = true;
                    foreach (Parent parent in thisTarget.ForeignKeys)
                    {
                        if (!que.Contains(parent.TableName))
                        {
                            allThere = false;
                        }
                    }

                    if (allThere)
                    {
                        //put this target in the que
                        que.Add(thisTarget.TableName);
                        //remove this target from the targets copy list
                        TargetsCopy.Remove(thisTarget);
                        //break
                        break;
                    }
                }
            }
            logger.Info("Finished sorting targets, the order is  \n");
            
            TargetsCopy = new List<Target>();
            foreach (string str in que)
            {
               logger.Info(str);
               TargetsCopy.Add(TargetTables.FirstOrDefault(t => t.TableName == str));              
            }
            this.SettingsModel.TargetTables = TargetsCopy;

        }

        



    }
}
