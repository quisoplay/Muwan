﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class NetworkResponse
    {
        public Column[][] Data { get; set; }
        public string []Errors { get; set; }

        public bool HasErrors { 
            get {
                if (Errors == null)
                {
                    return false;
                }

                if (Errors.Length > 0)
                {
                    return true;
                }

                return false;
            } 
        }

        public string GetErrors {
            get
            {
                if (HasErrors)
                {
                    string str = "";
                    for (int i = 0; i < Errors.Length; i++)
                    {
                        str = str + Errors[i] + " \n";
                    }
                    return str;
                }
                return "";
            }
        }
    }
}
