﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class SettingsModel
    {
        public bool IsBranched { get; set; }

        public string EndPoint { get; set; }

        public int BranchId { get; set; }

        public string DatabaseUser { get; set; }

        public string CleanDatabaseUser { get; set; }

        public string IdType { get; set; }

        public long TallyWatcherInterval { get; set; }
        public long DraftWatcherInterval { get; set; }

        public string BaseDomain { get; set; }
        public string PersisterURL { get; set; }
        public string FetcherURL { get; set; }
        public string EqualiserURL { get; set; }
        public string ProgresserURL { get; set; }


        public string DataBaseType { get; set; }
        public string ConnectionString { get; set; }
        public string TallyTableName { get; set; }
        public string AdapterTableName { get; set; }
        public string EqualiseTableName { get; set; }

        public string CreateTallyTableQuery { get; set; }
        public string CreateAdapterTableQuery { get; set; }
        public string CreateEqualiseTableQuery { get; set; }

        public List<Target> TargetTables { get; set; }

        public string InsertTrigerQuery { get; set; }
        public string UpdateTrigerQuery { get; set; }
        public string DeleteTrigerQuery { get; set; }

        public string NextTallyQuery { get; set; }
        public string TallyFetchQuery { get; set; }
        public string CheckEqualisedTargetQuery { get; set; }
        public string InsertEqualisationTargetQuery { get; set; }
        public string CheckAdaptedQuery { get; set; }
        public string PersistNewRecordQuery { get; set; }
        public string InsertAdaptationQuery { get; set; }
        public string DeleteRecordQuery { get; set; }
        public string UpdateEqualisedTargetQuery { get; set; }
        public string CheckPersistedRecordQuery { get; set; }
        public string UpdateAdaptedQuery { get; set; }
        public string UpdateRecordQuery { get; set; }
        


        public bool SkipMissingRecord { get; set; }
    }
}
