﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class MuwanService
    {
        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Timer DraftWatcher;
        private Timer TallyWatcher;

        public MuwanService()
        {
            logger.Info("==>Created the service object");
        }

        public  void Start(string[] args)
        {
            logger.Info("==>Service started");
            SetUp();
        }

        public  void Stop()
        {
            logger.Info("==>Service stopped");
            Finish();
        }

        public  void Pause()
        {
            logger.Info("==>Service paused");
            Finish();
        }

        public  void Continue()
        {
            logger.Info("==>Service continued");
            SetUp();
        }

        private void SetUp()
        {
            //first load in the settings
            Setting.GetInstance().LoadSettings();
            //bootstrap the db
            Db.Init();
            //sort the targets
            Setting.GetInstance().SortTargets();
            //balance the tables
            Db.EquallizeTables();
            //initialise watchers
            TallyWatcher = new Timer(WatchTally, this, Setting.GetInstance().TallyWatcherInterval, Setting.GetInstance().TallyWatcherInterval);
            DraftWatcher = new Timer(WatchDraft, this, Setting.GetInstance().DraftWatcherInterval, Setting.GetInstance().DraftWatcherInterval);
            
        }

        private void Finish()
        {
            //stop the watchers
            TallyWatcher.Dispose();
            DraftWatcher.Dispose();
            //remove watchers
            TallyWatcher = null;
            DraftWatcher = null;
            //dispose the settings object
            Setting.DisposeInstance();
        }

        private void WatchTally(Object sender)
        {
            String logStr = "==>Tally watcher action \n";
            logger.Info(logStr);
            //connect to the data base
            Db db = new Db();
            db.Connect();
            //get the next available tally row
            Tally tally = db.NextTally();
            if (tally != null)
            {
                //get the target table
                Target target = Setting.GetInstance().GetTargetTable(tally.affected_table);
                if (target == null)
                {
                    //this is not acceptable
                    logStr = "Exception in TallyWatcher : Could not find the target table to fetch records";
                    logger.Info(logStr);
                    throw new Exception(logStr);
                }
                if(tally.activity.Equals(Convert.ToString(Setting.TallyActivities.input)) || tally.activity.Equals(Convert.ToString(Setting.TallyActivities.modify))){

                    //fetch the tallied record
                    String query = Setting.GetCorrectedTallyFetchQuery(target.TableName, target.UniqueColumnName, tally.unique_value);
                    Column[] row = db.GetRecord(query);
                    if (row != null && row.Length > 0)
                    {
                        //adapt record for export
                        //go through all its parents
                        foreach (Parent parent in target.ForeignKeys)
                        {
                            bool foundAdaptation = false;
                            for (int j = 0; j < row.Length; j++)
                            {
                                if (row[j] != null && !String.IsNullOrEmpty(row[j].Name) && row[j].Name.Equals(parent.ColumnName))
                                {
                                    //get this ones adaptation basing on this local value
                                    Adapted adapted = Db.GetAdapted(parent.TableName, row[j].Value, false );
                                    if (adapted != null)
                                    {
                                        //replace the data with the foreign value
                                        row[j].Value = adapted.foreign_table_unique_value;
                                        foundAdaptation = true;
                                        break;
                                    }
                                }
                            }
                            if(foundAdaptation==false){
                                //this is not acceptable
                                logStr = "Exception in TallyWatcher : Could not find the parent table adaptation, tally watcher will not continue to the next tally: ";
                                logStr = logStr + " target is " + target.TableName + " , parentColumn = " + parent.ColumnName + " , parent table = " + parent.TableName;
                                logger.Info(logStr);
                                throw new Exception(logStr);
                            }
                        }
                        //send this record to the server
                        //tally entries mark new records that only exist at this end point
                        //this means that before this is persisted to the main db, there is no adaptation record for it
                        Network network = new Network();
                        if(tally.activity.Equals(Convert.ToString( Setting.TallyActivities.input))){
                            string response = network.SendBranchRecordForTable(target.TableName, row, Setting.TallyActivities.input);
                            NetworkResponse networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                            //check if there were some errors 
                            if (networkResponse.HasErrors)
                            {
                                string info = "Tally persistence to server failed, request had errors : " + networkResponse.GetErrors;
                                logger.Info(info);
                                throw new Exception(info);
                            }
                            //update records adaptation
                            //get the new foreign key given to this record
                            string forignValue = networkResponse.Data[0][0].Value;
                            Adapted adaptation = new Adapted();
                            adaptation.table_name = target.TableName;
                            adaptation.local_table_unique_value = tally.unique_value;
                            adaptation.foreign_table_unique_value = forignValue;
                            bool results = Db.CreateAdaption(adaptation);
                            if (results)
                            {
                                //delete this tally record
                                Db.DeleteTally(Convert.ToString(tally.id));
                            }
                        }

                        if (tally.activity.Equals(Convert.ToString(Setting.TallyActivities.modify)))
                        {                         
                            //get this ones adaptation basing on this local value
                            Adapted adapted = Db.GetAdapted(target.TableName, tally.unique_value, false);
                            if (adapted == null)
                            {
                                string info = "Tally persistence to server to update failed, due missing adaptation record : " + target.TableName + "@" + tally.unique_value;
                                logger.Info(info);
                                throw new Exception(info);
                            }
                            
                            for (int j = 0; j < row.Length; j++)
                            {
                                if (row[j] != null && !String.IsNullOrEmpty(row[j].Name) && row[j].Name.Equals(target.UniqueColumnName))
                                {
                                    //replace the data with the foreign value
                                    row[j].Value = adapted.foreign_table_unique_value;
                                    break;
                                }
                            }

                            string response = network.SendBranchRecordForTable(target.TableName, row, Setting.TallyActivities.modify);
                            NetworkResponse networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                            //check if there were some errors 
                            if (networkResponse.HasErrors)
                            {
                                string info = "Tally persistence to server to update failed, request had errors : " + networkResponse.GetErrors;
                                logger.Info(info);
                                throw new Exception(info);
                            }
                            
                            //delete this tally record
                            Db.DeleteTally(Convert.ToString(tally.id));
                            
                        }



                    }else{
                        //nyd
                        //what should happen if there is no record against this tally
                        //if the record was deleted locally it should be fine or foreign but for now i dont know what to do
                    }

              }else if(tally.activity.Equals(Convert.ToString(Setting.TallyActivities.remove))){
                  //update  to show that it was deleted locally and foreign after the newtwork returns
                  //get the adaptation for this record
                  Adapted adaptation = Db.GetAdapted(tally.affected_table, tally.unique_value, false);
                  if (adaptation != null)
                  {
                      adaptation.local_deleted = "yes";
                      bool resx = Db.UpdateAdapted(adaptation);
                      if (resx)
                      {
                          //inform the online database to delete this record
                          Network network = new Network();
                          Column []col = new Column[1];
                          col[0] = new Column(){ Name = target.UniqueColumnName, Value = adaptation.foreign_table_unique_value };
                          string response = network.SendBranchRecordForTable(tally.affected_table, col, Setting.TallyActivities.remove);
                          NetworkResponse networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                          //check if there were some errors 
                          if (networkResponse.HasErrors)
                          {
                              string info = "Tally persistence to server to delete record failed, request had errors : " + networkResponse.GetErrors;
                              logger.Info(info);
                              throw new Exception(info);
                          }
                          //make the final local adaptation update
                          adaptation.foreign_deleted = "yes";
                          resx = Db.UpdateAdapted(adaptation);
                          if (resx)
                          {
                              //delete this tally
                              Db.DeleteTally(Convert.ToString(tally.id));
                          }
                          else
                          {
                              string info = "Failed to updated tally to foreign_deleted = 'yes' for deletings " + tally.affected_table + "@" + tally.unique_value;
                              logger.Info(info);
                              throw new Exception(info);
                          }
                      }
                      else
                      {
                          string info = "Failed to updated tally to local_deleted = 'yes' for deletings " + tally.affected_table + "@" + tally.unique_value;
                          logger.Info(info);
                          throw new Exception(info);
                      }
                  }
                  else
                  {
                      string info = "Tally record to be deleted didnot have an adaptation record : " + tally.affected_table + "@" + tally.unique_value;
                      logger.Info(info);
                      throw new Exception(info);
                  }
              }

            }


            db.Disconnect();
            logger.Info(logStr);

        }

        public void WatchDraft(Object sender)
        {
            String logStr = "==>Tally watcher action \n";
            logger.Info(logStr);
            //ask the server if there is any data for me
            Network network = new Network();
            string response = network.FetchNextDraft();
            NetworkResponse networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
            //check if there were some errors 
            if (networkResponse.HasErrors)
            {
                string info = "Tally persistence to server to delete record failed, request had errors : " + networkResponse.GetErrors;
                logger.Info(info);
                throw new Exception(info);
            }
            //check if there is any data available
            if (networkResponse.Data == null || networkResponse.Data.Length == 0)
            {
                //well just do nothing
            }
            else
            {
                //connect to the data base
                Db db = new Db();
                db.Connect();

                //check the activity
                Setting.TallyActivities activity = Setting.TallyActivities.input;
                bool activityFound = false;
                for (int j = 0; j < networkResponse.Data[0].Length; j++)
                {
                    if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals("muwan_activity"))
                    {
                        //replace the data with the foreign value
                        if (networkResponse.Data[0][j].Value == Convert.ToString(Setting.TallyActivities.input))
                        {
                            activity = Setting.TallyActivities.input;
                            activityFound = true;
                        }
                        else if (networkResponse.Data[0][j].Value == Convert.ToString(Setting.TallyActivities.modify))
                        {
                            activity = Setting.TallyActivities.modify;
                            activityFound = true;
                        }
                        else if (networkResponse.Data[0][j].Value == Convert.ToString(Setting.TallyActivities.remove))
                        {
                            activity = Setting.TallyActivities.remove;
                            activityFound = true;
                        }
                        else
                        {
                            string info = " Unknown activity:" + networkResponse.Data[0][j].Value + " from the server";
                            logger.Info(info);
                            throw new Exception(info);
                        }
                        break;
                    }
                }

                //if the activity is null then we have a problem
                if (activityFound == false)
                {
                    string info = " activity from the server could not be found from the server response data";
                    logger.Info(info);
                    throw new Exception(info);
                }

                //get the target table
                Target target = null;
                for (int j = 0; j < networkResponse.Data[0].Length; j++)
                {
                    if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals("muwan_table"))
                    {
                        String name = networkResponse.Data[0][j].Value;
                        target = Setting.GetInstance().TargetTables.FirstOrDefault(t => t.TableName == name);
                        break;
                    }
                }

                if (target == null)
                {
                    string info = "Target for the data from server is null";
                    logger.Info(info);
                    throw new Exception(info);
                }



                if (activity == Setting.TallyActivities.input)
                {
                    //////////////////////////
                    //check if the target has foreign keys
                    bool hasParents = false;
                    if (target.ForeignKeys != null && target.ForeignKeys.Count > 0)
                    {
                        hasParents = true;
                    }
                    string lastPersistedForeignValue = "";

                    //insert record for the table 
                    
                    //get the adapted values for the local environment if target has parents 
                    //and the type of ids used on the database are auto
                    //there is no need to adapt guid ids
                    if (hasParents && Setting.GetInstance().IdType == Setting.IdTypes.auto)
                    {
                        foreach (Parent parent in target.ForeignKeys)
                        {
                            //getting the foreign value, so that we can get the locally adapted value
                            for (int j = 0; j < networkResponse.Data[0].Length; j++)
                            {
                                if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals(parent.ColumnName))
                                {
                                    string foreignValue = networkResponse.Data[0][j].Value;
                                    Adapted adapted = Db.GetAdapted(parent.TableName, foreignValue, true);
                                    //depending on the settings, muwan will chock or forgive for missing data
                                    //if skip missing record is false, it means that it should not forgive
                                    //that is , it should chock
                                    if (adapted == null && Setting.GetInstance().SkipMissingRecord == false)
                                    {
                                        string info = "Exception in trying to enter new  data, at the adaptation stage, there is missing data";
                                        logger.Info(info);
                                        throw new Exception(info);
                                    }

                                    //adapt the data ,if adapted is not null, alse means that you have forgiven, according to the settings
                                    if (adapted != null)
                                    {
                                        //we are looking for the local value from the adaptation
                                        networkResponse.Data[0][j].Value = adapted.local_table_unique_value;
                                        break;
                                    }
                                    
                                }

                            }

                        }
                    }//end of adapting

                    //after dapting this row, then it can be inserted, if it does not already exist
                    Column[] dataToPersist = new Column[networkResponse.Data[0].Length - 3]; // -3 because of the muwan_table , muwan_draft_id, and muwan_activity
                    for(int i=0; i< dataToPersist.Length; i++){
                        if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) && 
                            (networkResponse.Data[0][i].Name.Equals("muwan_table")  || 
                             networkResponse.Data[0][i].Name.Equals("muwan_activity") ||
                             networkResponse.Data[0][i].Name.Equals("muwan_draft_id") ))
                        {
                            continue;
                        }
                        dataToPersist[i] = networkResponse.Data[0][i];
                    }
                    Boolean exists = Db.WasRecordAlreadyPersisted(target, dataToPersist); 
                    if (exists == false)
                    {
                        //this step will also creat an adaptation for this record
                        Db.PersistNewRecord(target, dataToPersist);
                        for (int j = 0; j < dataToPersist.Length; j++)
                        {
                            if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals(target.UniqueColumnName))
                            {
                                //we are looking for the foreign value
                                lastPersistedForeignValue = networkResponse.Data[0][j].Value;
                                break;
                            }
                        }
                    }
                    else
                    {
                        string infox = "Record already exists for table " + target.TableName;
                        infox = infox + " : data is " + JsonConvert.SerializeObject(networkResponse.Data[0]);
                        logger.Info(infox);
                    }

                    

                    //if all the records have been persisted then we can mark this off as finished persisting
                    if (!String.IsNullOrEmpty(lastPersistedForeignValue))
                    {
                        //tell the server to make this endpoints progress move forward
                        string lastDraftId = "0";
                        for (int i = 0; i < networkResponse.Data[0].Length; i++)
                        {
                            if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) &&
                                networkResponse.Data[0][i].Name.Equals("muwan_draft_id"))
                            {
                                lastDraftId = networkResponse.Data[0][i].Value;
                                break;
                            }
                        }

                        response = network.MakeProgress(lastDraftId);
                        networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                        //check if there were some errors 
                        if (networkResponse.HasErrors)
                        {
                            string info = "Marking progress produced errors : " + networkResponse.GetErrors;
                            logger.Info(info);
                            throw new Exception(info);
                        }

                    }
                            
                    //////////////////////////
                }else if(activity == Setting.TallyActivities.modify){
                    ///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
                    //check if the target has foreign keys
                    bool hasParents = false;
                    if (target.ForeignKeys != null && target.ForeignKeys.Count > 0)
                    {
                        hasParents = true;
                    }
                    string lastPersistedForeignValue = "";

                    //update record for the table 

                    //get the adapted values for the local environment if target has parents 
                    //and the type of ids used on the database are auto
                    //there is no need to adapt guid ids
                    if (hasParents && Setting.GetInstance().IdType == Setting.IdTypes.auto)
                    {
                        foreach (Parent parent in target.ForeignKeys)
                        {
                            //getting the foreign value, so that we can get the locally adapted value
                            for (int j = 0; j < networkResponse.Data[0].Length; j++)
                            {
                                if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals(parent.ColumnName))
                                {
                                    string foreignValue = networkResponse.Data[0][j].Value;
                                    Adapted adapted = Db.GetAdapted(parent.TableName, foreignValue, true);
                                    //depending on the settings, muwan will chock or forgive for missing data
                                    //if skip missing record is false, it means that it should not forgive
                                    //that is , it should chock
                                    if (adapted == null && Setting.GetInstance().SkipMissingRecord == false)
                                    {
                                        string info = "Exception in trying to update data from server, at the adaptation stage, there is missing data";
                                        logger.Info(info);
                                        throw new Exception(info);
                                    }

                                    //adapt the data ,if adapted is not null, alse means that you have forgiven, according to the settings
                                    if (adapted != null)
                                    {
                                        //we are looking for the local value from the adaptation
                                        networkResponse.Data[0][j].Value = adapted.local_table_unique_value;
                                        break;
                                    }

                                }

                            }

                        }
                    }//end of adapting

                    //after dapting this row, then it can be updated, if it does already exist
                    Column[] dataToPersist = new Column[networkResponse.Data[0].Length - 3]; // -3 because of the muwan_table , muwan_draft_id, and muwan_activity
                    for (int i = 0; i < dataToPersist.Length; i++)
                    {
                        if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) &&
                            (networkResponse.Data[0][i].Name.Equals("muwan_table") ||
                             networkResponse.Data[0][i].Name.Equals("muwan_activity") ||
                             networkResponse.Data[0][i].Name.Equals("muwan_draft_id")))
                        {
                            continue;
                        }
                        dataToPersist[i] = networkResponse.Data[0][i];
                    }

                    //adapt this record
                    bool exists = false;
                    for (int j = 0; j < dataToPersist.Length; j++)
                    {
                        if (dataToPersist[j] != null && !String.IsNullOrEmpty(dataToPersist[j].Name) && dataToPersist[j].Name.Equals(target.UniqueColumnName))
                        {
                            string value = dataToPersist[j].Value;
                            Adapted adaptation = Db.GetAdapted(target.TableName, value, true); //because this is from online
                            if (adaptation == null)
                            {
                                string info = "Exception in trying to update data from server, at the adaptation stage, there is no adaptation";
                                logger.Info(info);
                                throw new Exception(info);
                            }
                            exists = true;
                            dataToPersist[j].Value = adaptation.local_table_unique_value;
                        }
                    }



                    if (exists == false)
                    {
                        string infox = "Record does not exists for table " + target.TableName;
                        infox = infox + " : data is " + JsonConvert.SerializeObject(networkResponse.Data[0]);
                        logger.Info(infox);
                        throw new Exception(infox);
                    }

                    
                    //this step will update this record
                    Db.UpdateRecord(target, dataToPersist);
                    for (int j = 0; j < dataToPersist.Length; j++)
                    {
                        if (networkResponse.Data[0][j] != null && !String.IsNullOrEmpty(networkResponse.Data[0][j].Name) && networkResponse.Data[0][j].Name.Equals(target.UniqueColumnName))
                        {
                            //we are looking for the foreign value
                            lastPersistedForeignValue = networkResponse.Data[0][j].Value;
                            break;
                        }
                    }
                    
                    



                    //if all the records have been persisted then we can mark this off as finished persisting
                    if (!String.IsNullOrEmpty(lastPersistedForeignValue))
                    {
                        //tell the server to make this endpoints progress move forward
                        string lastDraftId = "0";
                        for (int i = 0; i < networkResponse.Data[0].Length; i++)
                        {
                            if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) &&
                                networkResponse.Data[0][i].Name.Equals("muwan_draft_id"))
                            {
                                lastDraftId = networkResponse.Data[0][i].Value;
                                break;
                            }
                        }

                        response = network.MakeProgress(lastDraftId);
                        networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                        //check if there were some errors 
                        if (networkResponse.HasErrors)
                        {
                            string info = "Marking progress produced errors in updating draft data : " + networkResponse.GetErrors;
                            logger.Info(info);
                            throw new Exception(info);
                        }

                    }

                    ///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
                }
                else if (activity == Setting.TallyActivities.remove)
                {
                    ///rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
                    //get the id to be deleted
                    string id = null;
                    for (int i = 0; i < networkResponse.Data[0].Length; i++)
                    {
                        if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) &&
                            networkResponse.Data[0][i].Name.Equals(target.UniqueColumnName))
                        {
                            id = networkResponse.Data[0][i].Value;
                            break;
                        }
                    }
                    if (id == null)
                    {
                        string info = "Exception there is not unique id for the record to delete  : " + target.TableName;
                        logger.Info(info);
                        throw new Exception(info);
                    }
                    //get the adaptation for this record
                    Adapted adaptation = Db.GetAdapted(target.TableName, id, true);
                    //if adaptation is null it means that there is no local record for this online record
                    //there for there would be no need to delete anything
                    if(adaptation != null){

                        bool res = Db.DeleteRecord(target, adaptation.local_table_unique_value);
                        if (res)
                        {
                            adaptation.local_deleted = "yes";
                            bool resx = Db.UpdateAdapted(adaptation);
                            if (resx)
                            {

                                //make progress
                                string lastDraftId = "0";
                                for (int i = 0; i < networkResponse.Data[0].Length; i++)
                                {
                                    if (networkResponse.Data[0][i] != null && !String.IsNullOrEmpty(networkResponse.Data[0][i].Name) &&
                                        networkResponse.Data[0][i].Name.Equals("muwan_draft_id"))
                                    {
                                        lastDraftId = networkResponse.Data[0][i].Value;
                                        break;
                                    }
                                }

                                response = network.MakeProgress(lastDraftId);
                                networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(response);
                                //check if there were some errors 
                                if (networkResponse.HasErrors)
                                {
                                    string info = "Marking progress produced errors in delete draft data : " + networkResponse.GetErrors;
                                    logger.Info(info);
                                    throw new Exception(info);
                                }

                                adaptation.foreign_deleted = "yes";
                                resx = Db.UpdateAdapted(adaptation);
                                if (resx == false)
                                {
                                    string info = "Failed to update adaptation to foreign deleted = 'yes' ";
                                    logger.Info(info);
                                    throw new Exception(info);
                                }
                            }
                            else
                            {
                                string info = "Failed to update adaptation to local deleted = 'yes' ";
                                logger.Info(info);
                                throw new Exception(info);
                            }
                        }
                    }
                    
                    ///rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
                }

                db.Disconnect();
                logger.Info(logStr);

            }

        }


    }
}
