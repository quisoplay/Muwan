﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using NLog;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace MuwanConsole
{
    class Db
    {
        //logs for this class
        private static Logger logger = LogManager.GetCurrentClassLogger();

        MySqlConnection mysqlConnection;

        /// <summary>
        ///     Init
        ///     Bootstraps the necessary tables and triggers
        ///     It makes sure that these are available on the client's local computer
        /// </summary>
        public static void Init()
        {
            bool hadError = false;
            logger.Info("==>Db Init method , bootstraping the db \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    //create equalise table
                    string query = Setting.GetInstance().CreateEqualiseTableQuery;
                    string info = "Executing create equalise table query: " + query;
                    logger.Info(info);
                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();

                    //create tally table
                    query = Setting.GetInstance().CreateTallyTableQuery;
                    info = "Executing create tally table query: " + query;
                    logger.Info(info);
                    thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();

                    //create adapter table if id type is auto
                    if (Setting.GetInstance().IdType == Setting.IdTypes.auto)
                    {
                        logger.Info("Muwan id type is auto ");
                        query = Setting.GetInstance().CreateAdapterTableQuery;
                        info = "Executing create adapter table query: " + query;
                        logger.Info(info);
                        thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                        thisMysqlCommand.ExecuteNonQuery();
                    }else{
                        logger.Info("Muwan id type is not auto, so no adapter table created ");
                    }

                    //create insert triggers on target tables to be synchronised
                    logger.Info("Creating insert triggers on target tables");
                    string triggerQuery = Setting.GetInstance().InsertTrigerQuery;
                    foreach (Target target in Setting.GetInstance().TargetTables)
                    {
                        query = triggerQuery.Replace("tableName",target.TableName);
                        query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                        info = "Executing create insert trigger on table " + target.TableName + ": query: " + query;
                        logger.Info(info);
                        //query = MySqlHelper.EscapeString(query);
                        //info = "Executing create trigger on table " + target.TableName + ": escaped query: " + query;
                        //logger.Info(info);
                        thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                        thisMysqlCommand.ExecuteNonQuery();
                    }

                    //create update triggers on target tables to be synchronised
                    logger.Info("Creating update triggers on target tables");
                    triggerQuery = Setting.GetInstance().UpdateTrigerQuery;
                    foreach (Target target in Setting.GetInstance().TargetTables)
                    {
                        query = triggerQuery.Replace("tableName", target.TableName);
                        query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                        info = "Executing create update trigger on table " + target.TableName + ": query: " + query;
                        logger.Info(info);
                        //query = MySqlHelper.EscapeString(query);
                        //info = "Executing create trigger on table " + target.TableName + ": escaped query: " + query;
                        //logger.Info(info);
                        thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                        thisMysqlCommand.ExecuteNonQuery();
                    }

                    //create delete triggers on target tables to be synchronised
                    logger.Info("Creating delete triggers on target tables");
                    triggerQuery = Setting.GetInstance().DeleteTrigerQuery;
                    foreach (Target target in Setting.GetInstance().TargetTables)
                    {
                        query = triggerQuery.Replace("tableName", target.TableName);
                        query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                        info = "Executing create delete trigger on table " + target.TableName + ": query: " + query;
                        logger.Info(info);
                        //query = MySqlHelper.EscapeString(query);
                        //info = "Executing create trigger on table " + target.TableName + ": escaped query: " + query;
                        //logger.Info(info);
                        thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                        thisMysqlCommand.ExecuteNonQuery();
                    }


                    //string ssh = "INSERT INTO `muwan_school` (`id`, `name`, `description`, `branch_id`) VALUES (NULL, 'tested', 'testeder','1');";
                    //thisMysqlCommand = new MySqlCommand(ssh, thisMysqlConnection);
                    //thisMysqlCommand.ExecuteNonQuery();


                    logger.Info("Finished initialisation ");
                    
                }
                catch (Exception ex)
                {
                    string info = "Exception while bootstraping db :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }finally{
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }
              
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }
        }


        public static bool DeleteTally(string id)
        {
            
            bool hadError = false;
            logger.Info("==>Db DeleteTally method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    
                    string query = Setting.GetInstance().DeleteRecordQuery;
                    query = query.Replace("tablename", Setting.GetInstance().TallyTableName);
                    query = query.Replace("tableUniqueColomnName", "id");
                    query = query.Replace("localValue", id);
                    string info = "delete tally query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();
                    

                    logger.Info("Finished deleting query ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while deleting query :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return true;

        }

        public static bool DeleteRecord(Target target, string uniqueIdValue)
        {

            bool hadError = false;
            logger.Info("==>Db DeleteRecord method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();


                    string query = Setting.GetInstance().DeleteRecordQuery;
                    query = query.Replace("tablename", target.TableName);
                    query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                    query = query.Replace("localValue", uniqueIdValue);
                    string info = "delete tally query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();


                    logger.Info("Finished deleting query ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while deleting query :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return true;

        }

        public static bool IsTargetTableEqualised(string tablename)
        {
            bool res = false;
            bool hadError = false;
            logger.Info("==>Db EquallizeTables method , equalisation of target tables \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "==>Db IsTargetTableEqualised method, target table == " + tablename + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().CheckEqualisedTargetQuery;
                    query = query.Replace("tablename", tablename);
                    info = "check query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    MySqlDataReader reader = thisMysqlCommand.ExecuteReader();
                    if (reader.HasRows == false)
                    {
                        logger.Info("There was no equalisation row for this table, one is going to be inserted");
                        //insert a row to tract this target table equalisation
                        query = Setting.GetInstance().InsertEqualisationTargetQuery;
                        query = query.Replace("tablename", tablename);
                        info = "insert query is :" + query;
                        logger.Info(info);
                        thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                        if (reader.IsClosed == false)
                        {
                            reader.Close();
                        }    
                        thisMysqlCommand.ExecuteNonQuery();
                        res = false;
                    }
                    else
                    {
                        while (reader.Read())
                        {
                            string str = reader.GetString("was_equallised");
                            res = str.Equals("false") ? false : true;
                            break;
                        }
                    }
                    if (reader.IsClosed == false)
                    {
                        reader.Close();
                    }
                    

                    logger.Info("Finished checking equalisation ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while checking  equalisation :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return res;

        }

        /// <summary>
        ///     This methos is not responsible for creating an adaptation record for the data
        ///     but rather it just retrieves
        ///     All records create an adaptation at the point when theiy are inserted into db
        /// </summary>
        /// <param name="tablename"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static Adapted GetAdapted(string tablename, string checkValue, bool isCheckValueForeign)
        {
            Adapted adapted = null;
            bool hadError = false;
            logger.Info("==>Db GetAdapted method , datapted object of target  \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + tablename + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().CheckAdaptedQuery;
                    query = query.Replace("tablename", tablename);
                    if (isCheckValueForeign)
                    {
                        query = query.Replace("direction", "foreign_table_unique_value");
                    }
                    else
                    {
                        query = query.Replace("direction", "local_table_unique_value");
                    }
                    query = query.Replace("!", checkValue);
                    info = "check adapted query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    MySqlDataReader reader = thisMysqlCommand.ExecuteReader();
                    if (reader.HasRows == false)
                    {
                        info = "No datapting row found for this target table " + tablename;
                        logger.Info(info);
                        
                        //it seems like this target table has no adaptation
                       
                    }
                    else
                    {
                        while (reader.Read())
                        {
                            adapted = new Adapted() { table_name = tablename };
                            adapted.id = reader.GetInt32("id");
                            adapted.local_table_unique_value = reader.GetString("local_table_unique_value");
                            adapted.foreign_table_unique_value = reader.GetString("foreign_table_unique_value");
                            adapted.local_deleted = reader.GetString("local_deleted");
                            adapted.foreign_deleted = reader.GetString("foreign_deleted");
                            adapted.date_created = reader.GetString("date_created");

                        }
                    }
                    reader.Close();


                    logger.Info("Finished getting adapted ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while checking  getting adapted :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return adapted;
        }


        public static bool UpdateAdapted(Adapted adapted)
        {
            bool hadError = false;
            logger.Info("==>Db UpdateAdapted method , datapted object of target  \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + adapted.table_name + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().UpdateAdaptedQuery;
                    query = query.Replace("localdeleted", adapted.local_deleted);
                    query = query.Replace("foreigndeleted", adapted.foreign_deleted);
                    query = query.Replace("!", Convert.ToString(adapted.id));
                    
                    info = "update adapted query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();



                    string j = "Finished updating adapted id=" + adapted.id;
                    logger.Info(j);

                }
                catch (Exception ex)
                {
                    string info = "Exception while updataing adapted :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return true;
        }

        /// <summary>
        ///     This method assumes that every new record from the main online database 
        ///     has to be adapted during the persistance stage
        /// </summary>
        /// <param name="target"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static bool WasRecordAlreadyPersisted(Target target, Column[] row)
        {
            bool res = false;
            bool hadError = false;
            logger.Info("==>Db WasRecordAlreadyPersisted method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + target.TableName + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().CheckPersistedRecordQuery;
                    query = query.Replace("tablename", target.TableName);
                    string cols = "";
                    string values = "";
                    string foreignUniqueValue = "";
                    for (int i = 0; i < row.Length; i++)
                    {
                        //avoid id if auto ids are used
                        if (Setting.GetInstance().IdType == Setting.IdTypes.auto && row[i].Name.Equals(target.UniqueColumnName))
                        {
                            foreignUniqueValue = row[i].Value;
                            break;
                        }
                    }
                    if (string.IsNullOrEmpty(foreignUniqueValue))
                    {
                        info = "Foreign key value not found when checking for record already persisted" ;
                        logger.Info(info);
                        throw new Exception(info);
                    }
                    query = query.Replace("foreignValue", foreignUniqueValue);

                    info = "check record exists query is :" + query;
                    logger.Info(info);

                    
                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    MySqlDataReader reader =   thisMysqlCommand.ExecuteReader();
                    if (reader.HasRows == false)
                    {
                        while (reader.Read())
                        {
                            //we are looking for a value in the local_table_unique_value
                            string value = reader.GetString("local_table_unique_value");
                            if (!String.IsNullOrEmpty(value))
                            {
                                res = true;
                                break;
                            }
                        }

                    }
                    else
                    {

                        res = false;
                    }
                    reader.Close();  

                    logger.Info("Finished checking for existence of new record ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while checking checking existence of new record :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return res;
        }

        public static bool CreateAdaption(Adapted adaptation)
        {
            bool hadError = false;
            logger.Info("==>Db CreateAdaption method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + adaptation.table_name + " \n";
                    logger.Info(info);
                   
                   
                    //this is the local value
                    string localUniqueValue = adaptation.local_table_unique_value;

                    string query = Setting.GetInstance().InsertAdaptationQuery;
                    query = query.Replace("tablename", adaptation.table_name);
                    query = query.Replace("localValue", localUniqueValue);
                    query = query.Replace("foreignValue", adaptation.foreign_table_unique_value);
                    info = "adaptation  query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    //should this process refuse, then the persisted record should be deleted and the whole
                    //process is a failure
                    try
                    {
                        thisMysqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        hadError = true;
                        throw new Exception("Failed to insert adaptation for record");
                    }


                    logger.Info("Finished adapting exixting record ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while  adapted existing record :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            return true;

        }

        public static void PersistNewRecord(Target target, Column[] row)
        {
            String res = null;
            bool hadError = false;
            logger.Info("==>Db PersistNewRecord method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + target.TableName + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().PersistNewRecordQuery;
                    query = query.Replace("tablename", target.TableName);
                    string cols = "";
                    string values = "";
                    string foreignUniqueValue = "";
                    for (int i = 0; i < row.Length; i++)
                    {
                        //avoid id if auto ids are used
                        if (Setting.GetInstance().IdType == Setting.IdTypes.auto && row[i].Name.Equals(target.UniqueColumnName) )
                        {
                            foreignUniqueValue = row[i].Value;
                            continue;
                        }
                        cols = cols + row[i].Name;
                        values = values + "'" + row[i].Value + "'";
                        if (i + 1 != row.Length)//this means that the next iteration will not fail
                        {
                            cols = cols + ", ";
                            values = values + ", ";
                        }
                    }
                    query = query.Replace("colums", cols);
                    query = query.Replace("entries", values);

                    info = "persist query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();
                    
                    //get the last inserted id for this record
                    //this is the local value
                    string localUniqueValue = Convert.ToString( thisMysqlCommand.LastInsertedId );

                    //this values are used to create the adapter record for this table
                    logger.Info("Create an adaptation for this record");
                    query = Setting.GetInstance().InsertAdaptationQuery;
                    query = query.Replace("tablename", target.TableName);
                    query = query.Replace("localValue", localUniqueValue);
                    query = query.Replace("foreignValue", foreignUniqueValue);
                    info = "adaptation  query is :" + query;
                    logger.Info(info);
                    thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    //should this process refuse, then the persisted record should be deleted and the whole
                    //process is a failure
                    try
                    {
                        thisMysqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        info = "deleting persisted record";
                        logger.Info(info);
                        query = Setting.GetInstance().DeleteRecordQuery;
                        query = query.Replace("tablename", target.TableName);
                        query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                        query = query.Replace("localValue", localUniqueValue);
                        info = "delete record  query is :" + query;
                        logger.Info(info);
                        thisMysqlCommand.ExecuteNonQuery();
                        //roll back changes
                        throw new Exception("Failed to insert adaptation for record");
                    }


                    logger.Info("Finished persisting new record ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while checking  getting adapted :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }

            
        }


        public static void UpdateRecord(Target target, Column[] row)
        {
            String res = null;
            bool hadError = false;
            logger.Info("==>Db UpdateRecord method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + target.TableName + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().UpdateRecordQuery;
                    query = query.Replace("tablename", target.TableName);
                    query = query.Replace("tableUniqueColomnName", target.UniqueColumnName);
                    
                    string cols = "";
                    string values = "";
                    string localUniqueValue = "";
                    for (int i = 0; i < row.Length; i++)
                    {
                        //avoid id if auto ids are used
                        if (Setting.GetInstance().IdType == Setting.IdTypes.auto && row[i].Name.Equals(target.UniqueColumnName))
                        {
                            localUniqueValue = row[i].Value;
                            continue;
                        }
                        cols = cols + row[i].Name + " = '"+ row[i].Value  +"'";
                        if (i + 1 != row.Length)//this means that the next iteration will not fail
                        {
                            cols = cols + ", ";
                        }
                    }

                    query = query.Replace("!", localUniqueValue);
                    query = query.Replace("entries", cols);

                    info = "update query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();

                    

                    logger.Info("Finished updating record ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while updating record :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }


        }


        public static void UpdateEqualisedTable(Target target, string value, string lastUnqueValue)
        {
            String res = null;
            bool hadError = false;
            logger.Info("==>Db UpdateEqualisedTable method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                MySqlConnection thisMysqlConnection = new MySqlConnection();
                try
                {
                    thisMysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    thisMysqlConnection.Open();

                    string info = "target table == " + target.TableName + " \n";
                    logger.Info(info);
                    string query = Setting.GetInstance().UpdateEqualisedTargetQuery;
                    query = query.Replace("status", value);
                    query = query.Replace("uniqueValue", lastUnqueValue);
                    query = query.Replace("tablename", target.TableName);

                    info = "update equalisation query is :" + query;
                    logger.Info(info);

                    MySqlCommand thisMysqlCommand = new MySqlCommand(query, thisMysqlConnection);
                    thisMysqlCommand.ExecuteNonQuery();

                    info = "Finished updating equalisation for  the table " + target.TableName;
                    logger.Info(info);

                }
                catch (Exception ex)
                {
                    string info = "Exception while updating equalisation :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    if (thisMysqlConnection != null && thisMysqlConnection.State != System.Data.ConnectionState.Closed)
                    {
                        thisMysqlConnection.Close();
                    }
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }


        }

        public static void EquallizeTables()
        {
            bool hadError = false;
            logger.Info("==>Db EquallizeTables method , equalisation of target tables \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                
                try
                {
                    

                    //get data for each target table
                    //we asume here that the targets have been sorted
                    foreach (Target target in Setting.GetInstance().TargetTables)
                    {
                        //check if this target was already equallised, if not then continue equalisation
                        bool wasEqualised = Db.IsTargetTableEqualised(target.TableName);
                        if (wasEqualised == false)
                        {
                            string lastUniqueValue = "0";
                            //get the tables records from the online database
                            Network network = new Network();
                            string records = network.GetBranchRecordsForTable(target.TableName, lastUniqueValue);
                            //convert the string of records to an array of lists of columns
                            NetworkResponse networkResponse = JsonConvert.DeserializeObject<NetworkResponse>(records);
                            //check if there were some errors 
                            if (networkResponse.HasErrors)
                            {
                                string info = "Request had errors : " + networkResponse.GetErrors;
                                logger.Info(info);
                                throw new Exception(info);
                            }
                            //check if the target has foreign keys
                            bool hasParents = false;
                            if (target.ForeignKeys != null && target.ForeignKeys.Count > 0)
                            {
                                hasParents = true;
                            }
                            string lastPersistedForeignValue = "";

                           

                                //insert records for the table 
                                for (int i = 0; i < networkResponse.Data.Length; i++)
                                {
                                    //get the adapted values for the local environment if target has parents 
                                    //and the type of ids used on the database are auto
                                    //there is no need to adapt guid ids
                                    if (hasParents && Setting.GetInstance().IdType == Setting.IdTypes.auto)
                                    {
                                        foreach (Parent parent in target.ForeignKeys)
                                        {                                          
                                            //getting the foreign value, so that we can get the locally adapted value
                                            for (int j = 0; j < networkResponse.Data[i].Length; j++)
                                            {
                                                if (networkResponse.Data[i][j] != null && !String.IsNullOrEmpty(networkResponse.Data[i][j].Name) && networkResponse.Data[i][j].Name.Equals(parent.ColumnName))
                                                {
                                                   
                                                    string foreignValue = networkResponse.Data[i][j].Value;
                                                    Adapted adapted = Db.GetAdapted(parent.TableName, foreignValue, true);
                                                    //depending on the settings, muwan will chock or forgive for missing data
                                                    //if skip missing record is false, it means that it should not forgive
                                                    //that is , it should chock
                                                    if (adapted == null && Setting.GetInstance().SkipMissingRecord == false)
                                                    {
                                                        string info = "Exception in trying to equalise data, at the adaptation stage, there is missing data";
                                                        logger.Info(info);
                                                        throw new Exception(info);
                                                    }



                                                    //adapt the data ,if adapted is not null, alse means that you have forgiven, according to the settings
                                                    if (adapted != null)
                                                    {
                                                        //we are looking for the local value from the adaptation
                                                        networkResponse.Data[i][j].Value = adapted.local_table_unique_value;
                                                        break;
                                                    }
                                                }
                                            }
                                            
                                        }

                                    }

                                    //after dapting this row, then it can be inserted, if it does not already exist
                                    Boolean exists = Db.WasRecordAlreadyPersisted(target, networkResponse.Data[i]);
                                    if (exists == false)
                                    {
                                        Db.PersistNewRecord(target, networkResponse.Data[i]);
                                        for (int j = 0; j < networkResponse.Data[i].Length; j++)
                                        {
                                            if (networkResponse.Data[i][j] != null && !String.IsNullOrEmpty(networkResponse.Data[i][j].Name) && networkResponse.Data[i][j].Name.Equals(target.UniqueColumnName))
                                            {
                                                //we are looking for the foreign value
                                                lastPersistedForeignValue = networkResponse.Data[i][j].Value;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string infox = "Record already exists for table " + target.TableName;
                                        infox = infox + " : data is " + JsonConvert.SerializeObject(networkResponse.Data[i]);
                                        logger.Info(infox);
                                    }

                                }

                                //if all the records have been persisted then we can mark this off as finished equalisation
                                if (!String.IsNullOrEmpty(lastPersistedForeignValue))
                                {
                                    Db.UpdateEqualisedTable(target, "true", lastPersistedForeignValue);
                                }
                            
                            
                        }
                    }
                   
                    

                    logger.Info("Finished equalisation of tables ");

                }
                catch (Exception ex)
                {
                    string info = "Exception while bootstraping db :" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
                finally
                {
                    
                }

            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }
        }

        public void Connect()
        {
            bool hadError = false;
            logger.Info("==>Db connection method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                try
                {
                    if (mysqlConnection == null)
                    {
                        mysqlConnection = new MySqlConnection();
                        mysqlConnection.ConnectionString = Setting.GetInstance().ConnectionString;
                    }

                    if (mysqlConnection.State == System.Data.ConnectionState.Closed || 
                        mysqlConnection.State == System.Data.ConnectionState.Broken)
                    {
                        mysqlConnection.Open();
                    }

                   
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    string info = "Connecting to the database failed:" + ex.Message;
                    logger.Info(info);
                    logger.Error(ex);
                    hadError = true;
                }
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli))){           
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }

            if (hadError)
            {
                throw new Exception(Setting.StopErrorMessage);
            }
        }

        /// <summary>
        ///     Returns the next available tally
        /// </summary>
        /// <returns>
        ///     A tally object
        /// </returns>
        public Tally NextTally()
        {
            String logStr = "==>Next tally action \n";
            logger.Info(logStr); 
            //get the next tally query
            string query = Setting.GetInstance().NextTallyQuery;
            string info = "Executing get next tally query: " + query;
            logger.Info(info);
            MySqlCommand thisMysqlCommand = new MySqlCommand(query, mysqlConnection);
            MySqlDataReader reader =   thisMysqlCommand.ExecuteReader();
            Tally returnTally = null;
            while (reader.Read())
            {
                returnTally = new Tally();
                returnTally.activity = reader.GetString("activity");
                returnTally.affected_table = reader.GetString("affected_table");
                returnTally.date_created = Convert.ToString( reader.GetDateTime("date_created").Ticks);
                returnTally.unique_value = reader.GetString("unique_value");
                returnTally.id = reader.GetInt32("id");
            }
            reader.Close();           
            return returnTally;
        }

        /// <summary>
        ///     The following was very helpfull, just copied the code from there and added a few calls
        ///     http://stackoverflow.com/questions/5554472/create-json-string-from-sqldatareader
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Column[] GetRecord(String query)
        {
            Column[] cols = new Column[0];
            String logStr = "==>GetRecord \n";
            logger.Info(logStr);
            string info = "Executing get record query: " + query;
            logger.Info(info);

            MySqlCommand thisMysqlCommand = new MySqlCommand(query, mysqlConnection);

            // ... SQL connection and command set up, only querying 1 row from the table


            try
            {
                // read the row from the table
                MySqlDataReader reader = thisMysqlCommand.ExecuteReader();
                if (!reader.HasRows)
                {
                    return null;
                }
                reader.Read();

                int fieldcount = reader.FieldCount; // count how many columns are in the row
                cols = new Column[fieldcount]; // storage for column values
                Object[] values = new Object[fieldcount]; // storage for column values
                reader.GetValues(values); // extract the values in each column

                //generate the colums, so that we can be able to adapt this data for export
                for (int index = 0; index < fieldcount; index++)
                { // iterate through all columns

                    Column column = new Column();
                    column.Name = reader.GetName(index);
                    column.Value = Convert.ToString(values[index]);
                    cols[index] = column;
                }
              
                reader.Close();

            }
            catch (MySqlException sqlException)
            { // exception
                String exstr = "Exception in db getrecord method \n";
                logger.Info(sqlException);
            }
            finally
            {
                mysqlConnection.Close(); // close the connection
            }


            return cols;
        }

        

        public void Disconnect()
        {
            logger.Info("==>Db disconnection method \n");
            if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mssql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysql)))
            {
                
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.mysqli)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.oracle)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else if (Setting.GetInstance().DataBaseType.Equals(Convert.ToString(Setting.DataBaseTypes.psql)))
            {
                string info = "Support is not yet available :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Support for this data base type is not yet available");
            }
            else
            {
                string info = "Unsupported database type :" + Setting.GetInstance().DataBaseType;
                logger.Info(info);
                throw new Exception("Unsupported database type");
            }
        }

       
    }
}
