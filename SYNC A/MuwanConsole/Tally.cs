﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    class Tally
    {
        public int id { get; set; }
        public string affected_table { get; set; }
        public string activity { get; set; }
		public string unique_value { get; set; }
		public string date_created { get; set; }
    }
}
