﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    /// <summary>
    ///     A foreign key to be used in a target table
    /// </summary>
    class Parent
    {
        public string ColumnName { get; set; }
        /// <summary>
        /// This table must be among the ables to be synchronised
        /// </summary>
        public string TableName { get; set; }
    }
}
