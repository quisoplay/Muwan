﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuwanConsole
{
    /// <summary>
    /// A table to be synchronised
    /// </summary>
    class Target
    {
        public string TableName { get; set; }
        public string UniqueColumnName { get; set; }
        public List<Parent> ForeignKeys { get; set; }
    }
}
