# ![alt tag](https://github.com/quisoplay/Muwan/blob/master/logo2.png) MUWAN
An Open Source Background Data Synchronisation Service and Tool.	
Offline access in your system is only a configuration away

#Shout out
I dedicate this project to you my friend Samwell Muwanguzi.		
Thank you for being there for me.	
Thank you so much for your sacrifies towards our dream company Quiso Play Grounds.		
May God reward you with the desires of your heart.		

# What is muwan

Muwan is a background service at the core, that synchronises data between two databases.
Every feature of muwan is configurable via a setting.json file, which makes it very easy to use.
Well the databases may be of different technologies at the endpoints as long as the table structures remain identical

# Features

	-Supports databases that use Auto Incremented Ids and those that use GUIDs
	-Supports INSERT, UPDATE, DELETE database operations
	-Configure to synchronise only the tables that you need or the entire database
	-Supports REST URLs 
	-Support for JSON, XML and Soap, means for data transfer (JSON only currnetly implemented)
	-Synchronise files in folders
	-RealTime data synchronisation 
	-Scheduled data synchronisation
	-Accept or Reject records with missing parent data
	-Supports systems built for Branches of an organisation where the branch id info 
	 is stored on the tables that are necessary to indicate records for a specific branch
	-Synchronisation Logs
	-Email and Screen notifications if errors occur
	-Supports databases with columns that use sql key words	
	-Can run as a standalone application or embedded in your application via .dlls or packages etc	
	-Graphical User Interface for writting and editing your configurations

# Road map

	These outlines the vision of this project as the utlimate data synchronisation tool, that just works

	04/03/2016

	-To develope a website for the software, which should also be open source
	 The site will have :
	 1.a home page that explains MUWAN
	 2.a link to download the latest stable realise of muwan
	 3.a forum for discussions
	 4.a link for donations
	 5.a contributors page
	 6.a link to the wiki
	 7.a page that explains the general workflow of the software
	 8.a page were people who have successfully integrated muwan can register thier products
	   and displayed.
	 9.a page for support and links to diffrent resources and support providers and integrators of muwan

	-To acquire an Apache Software Liscence for the software

	-To develope a user manual for those that want to integrate muwan in their systems
	 Every  sync type e.g SYNC K, should have this manual as a pdf in the repository

	-To develope a technical manual for those that want to embedde muwan in their systems
	 Every  sync type e.g SYNC K, should have this manual as a pdf in the repository

	 -To create a very cool logo for MUWAN
	 
	 -To create social accounts for muwan 
	  Facebook Page
	  Google+ 
	  Twitter
	  Youtube channel
	 

# Supported technology combinations and databases [Proverbs 17:22]

	-"SYNC A" ==> C# dot net and MYSQL [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] (currently implemented)

	-"SYNC B" ==> C# dot net and MYSQL [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC C" ==> C# dot net and MSSQL [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC D" ==> C# dot net and MSSQL [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC E" ==> C# dot net and MSSQL [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC F" ==> C# dot net and MSSQL [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE] 

	-"SYNC G" ==> Java and MYSQL [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC H" ==> Java and MYSQL [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC I" ==> Java and MSSQL [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC J" ==> Java and MSSQL [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC K" ==> Java and MSSQL [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC L" ==> Java and MSSQL [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE] 

	-"SYNC M" ==> Android and MYSQLI [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC N" ==> Android and MYSQLI [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC O" ==> Android and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC P" ==> Android and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE] 
	
	-"SYNC Q" ==> Windows Phone and MYSQLI [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC R" ==> Windows Phone and MYSQLI [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC S" ==> Windows Phone and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC T" ==> Windows Phone and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE]
	
	-"SYNC U" ==> Cordova and MYSQLI [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC V" ==> Cordova and MYSQLI [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC W" ==> Cordova and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC X" ==> Cordova and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE] 
	
	-"SYNC Y" ==> IPhone and MYSQLI [CLIENT SIDE] with  PHP and MYSQL [SERVER SIDE] 

	-"SYNC Z" ==> IPhone and MYSQLI [CLIENT SIDE] with  PHP and MSSQL [SERVER SIDE] 

	-"SYNC ZA" ==> IPhone and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MSSQL [SERVER SIDE] 

	-"SYNC ZB" ==> IPhone and MYSQLI [CLIENT SIDE] with  C# ASP.NET and MYSQL [SERVER SIDE] 	

# Usage as a stand alone

	If you are using muwan as a stand alone application, then it does not matter what technology was used to develop
	your system but the database managment system and database type matter alot.
	In which case you have to just make the right configurations

# Usage as embedding in your application development

	Identify the right technology combination for your system, this could be for example "SYNC S"
	Then go to the wiki and find the chapter for your sync e.g "SYNC S"
	Follow the instructions

# Configurations

	[Client side]
	-IsBranched : default is true
	This indicates that the system being synchronised supports branches for the organisation
	by default it is true, pass false if other wise
	
	-BranchId : default is 0,
	The local machine indicates to what branch does it belong to via this branch id
	If there are no branches this value can be any integer but 0 is prefered

	-EndPoint : default is "4444",
	This is a string which uniquely identifies the computer or endpoint which is running muwan.
	This should be unique for all devices that are using muwan on your system

	-SkipMissingRecord : default is false,
	Muwan sorts the target tables (tables that need to be synchronised) in such a way that all parent tables
	come before their child tables.
	So when its synchronising data, it checks if this records parent refrences are already synchronised, so if
	this SkipMissingRecord setting is true and the parent data is not available, it chocks by throwing an
	error and then stop, but if is false, it continues to try to persist that record

	-DatabaseUser : "'muwan'@'%'", 
	You are required to create a user for the muwan service, this user should have permimssions to 
	alter database structure,
	Insert,
	Update,
	Delete
	 
	-CleanDatabaseUser : "muwan@localhost"
	This is the same as  DatabaseUser but the quotes are removed and the server is specified
	 
	-IdType : "auto"
	This is a string which specifies the type of ids that were used
	There are two types supported so far "Auto increamented Ids" and "GUIDs"

	-TallyWatcherInterval : 1000
	The tally watcher is a timer thread that watches for changes on the local database, this setting is
	how many milliseconds should ellapse before the thread checks for changes and commits then to the server

	-DraftWatcherInterval : 1000  
	The draft watcher is a timer thread that watches for changes on the online database, this settings is
	how many milliseconds should ellapse before the thread checks for changes and commits then to the local database

    -BaseDomain : "http://localhost/hotel/"
	The online domain where the server scripts of muwan are located
	     
    -PersisterURL : "muwan/persister.php"
	The url to the scripts that persist the data send by the tally watcher
	     
    -FetcherURL : "muwan/fetcher.php"
	The URL that is used by Muwan to fetch in data via the Draft watcher thread
	   
    -EqualiserURL : "muwan/equaliser.php"
	The URL used to fetch data from the server during the equalisation step

    -ProgresserURL : "muwan/progresser.php"
	When ever the Draft watcher persists changes to the localdatabase it tells the server how far this endpoint has gone
	The progress URL specifies resource location for the tracking progress of end points

    -RestURLExample : "muwan/equalise/{branchid}/{table}/{value}"
	You dont need to edit this, this one just shows you that REST URLs are supported

    -DataBaseType : "mysql"
	The type of database used
	       
	-ConnectionString : "server=localhost;user=muwan;database=muwan_local_db;port=3306;password=muwan;"
	The local connection string
	   
    -TallyTableName : "muwan_tally"
	The name of the table that keeps track of  the local changes
	      
    -AdapterTableName : "muwan_adapter"
	If auto ids are used there needs to be an adapter table, that is used in adapting ids

    -EqualiseTableName : "muwan_equalise"
	This table keeps track of the equalisation step

	-TargetTables : []
	An array that contains the targets that need to be equalised 
	e.g
	[{
    	"TableName" : "t5cym_event_tasks",
		"UniqueColumnName" : "id",
		"ForeignKeys" : [{
			"ColumnName" : "case_id",
			"TableName" : "t5cym_law_cases"
		}]
    },{
    	"TableName" : "t5cym_law_areas",
		"UniqueColumnName" : "id",
		"ForeignKeys" : []
    }]

	[Server side]

# Change Log

	# 04/3/2016
	-updated the readme.md file to be more imformative
	-Restructured the project into sync type directories, so each sync type will have its own changeLog.md